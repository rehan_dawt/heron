@isTest
public class SL_TestDataFactory {
    public static String currentTest = 'NULL';
    private static Map<Boolean, String> mapMessageByResult = new Map<Boolean, String>();
    private static Integer failures = 0;
    private static Integer passes = 0;

    public static String strUserName = 'testUserFromTestDataFactory@heron.com.test';
    private static String strUserProfileId = String.isNotBlank(getUserProfileId()) ? getUserProfileId() : [Select Id From Profile Limit 1].Id;
    private static String strUserRoleId = String.isNotBlank(getUserRoleId()) ? getUserRoleId() : [Select Id From UserRole Limit 1].Id;

    private static String getUserProfileId() {
        return strUserProfileId;
    }
    private static String getUserRoleId() {
        return strUserRoleId;
    }
    
    /*	Test Methods for Test Data Creation */
    
    //Using this function in :
    // 1. 
    public static void createTestDataForTask()
    {
    	Task objTask = new Task(Subject = 'Test Task');
		insert objTask;
        
    }
    
    //Using this function in :
    // 1. SL_TestRedirector
    public static void createTestDataForEvent()
    {
    	Task objTask = new Task(Subject = 'Test Task');
		insert objTask;
		
		Event objEvent = new Event();
	    objEvent.IsParent__c = true;
	    objEvent.StartDateTime = date.today();
	    objEvent.EndDateTime = date.today();
	    insert objEvent;
	    
	    SL_Meeting_Notes_Settings__c cs = new SL_Meeting_Notes_Settings__c(SL_Redirector__c = true);
    	insert cs;
		
    }
    
    //Using this function in :
    // 1. SL_TestAttachmentController
    public static void createTestDataForTaskEventAttachment()
    {
    	Task objTask = new Task(Subject = 'Test Task');
		insert objTask;

		Event objEvent = new Event(Subject = 'Test Event', StartDateTime = Datetime.now(), EndDateTime = Datetime.now());
		insert objEvent;

		SL_Meeting_Notes_Settings__c setting = new SL_Meeting_Notes_Settings__c(Maximum_Number_Of_Attachments_In_One_Go__c = 2);
		insert setting;
		
		Attachment objTaskAttachment = new Attachment(Name='Unit Test Task Attachment',
												Body=Blob.valueOf('Unit Test Task Attachment Body'),
												ParentId=objTask.Id);
        insert objTaskAttachment;

        Attachment objEventAttachment = new Attachment(Name='Unit Test Event Attachment',
												Body=Blob.valueOf('Unit Test Event Attachment Body'),
												ParentId=objEvent.Id);
        insert objEventAttachment;
        
        
    }
    
    public static void createTestDataForTopicManagement()
    {
    	Account a = new Account(Name = 'test');
        insert a;
        
        FeedItem fi = new FeedItem(Body = 'blah blah blah, I hate tests', ParentId = a.Id);
        insert fi;
        
        List<Topic> topics = new List<Topic>();
        topics.add(new Topic(Name = 'Test Topic 1'));
        topics.add(new Topic(Name = 'Test Topic 2'));
        topics.add(new Topic(Name = 'Test Topic 3'));
        topics.add(new Topic(Name = 'Test Topic 4'));
        insert topics;
        
        List<TopicAssignment> tas = new List<TopicAssignment>();
        tas.add(new TopicAssignment(TopicId = topics.get(0).Id, EntityId = fi.Id));
        tas.add(new TopicAssignment(TopicId = topics.get(1).Id, EntityId = fi.Id));
        tas.add(new TopicAssignment(TopicId = topics.get(2).Id, EntityId = fi.Id));
        insert tas;
        
        Approved_Topics__c at1 = new Approved_Topics__c(Name = 'Test Topic 1', Category__c = 'Featured', Id__c = topics.get(0).Id);
        insert at1;
        
        Approved_Topics__c at2 = new Approved_Topics__c(Name = 'Test Topic 4', Category__c = 'Featured', Id__c = topics.get(3).Id);
        insert at2;
    }
    
    public static void createTestDataForMultipleTaskEvent()
    {
    	Account objAcc = new Account(Name='Test Account');
		insert objAcc;

		Contact objCont = new Contact(LastName='Test Contact');
		insert objCont;

		List<Task> lstTask = new List<Task>{
			new Task(Subject = 'Test Task1', ActivityDate = Date.today(), WhatId=objAcc.Id, WhoId=objCont.Id),
			new Task(Subject = 'Test Task2', ActivityDate = Date.today()+1, WhatId=objAcc.Id, WhoId=objCont.Id),
			new Task(Subject = 'Test Task3', ActivityDate = Date.today()+1, WhatId=objAcc.Id, WhoId=objCont.Id),
			new Task(Subject = 'Test Task4', ActivityDate = Date.today()+1, WhatId=objAcc.Id, WhoId=objCont.Id)
		};

		insert lstTask;

		List<Event> lstEvent = new List<Event>{
			new Event(Subject = 'Test Event1', StartDateTime = Datetime.now().addMinutes(-3), EndDateTime = Datetime.now(), WhatId=objAcc.Id, WhoId=objCont.Id),
			new Event(Subject = 'Test Event2', StartDateTime = Datetime.now(), EndDateTime = Datetime.now(), WhatId=objAcc.Id, WhoId=objCont.Id),
			new Event(Subject = 'Test Event3', StartDateTime = Datetime.now(), EndDateTime = Datetime.now(), WhatId=objAcc.Id, WhoId=objCont.Id),
			new Event(Subject = 'Test Event4', StartDateTime = Datetime.now().addMinutes(-10), EndDateTime = Datetime.now(), WhatId=objAcc.Id, WhoId=objCont.Id)
		};

		insert lstEvent;
    }
    
    public static void createTestDataForNGForce()
    {
        Task objTask = new Task(isParent__c=true);
        insert objTask;
        
        Task objTask2 = new Task(ParentId__c=objTask.Id, OwnerId=UserInfo.getUserId());
        insert objTask2;

        Task objTask3 = new Task(isParent__c=true);
        insert objTask3;

        Task objTask4 = new Task(isParent__c=true);
        insert objTask4;

        Account objAccount = new Account(Name='Test');
        insert objAccount;

        Attachment objAttachment = new Attachment(Name='Test Attachment', Body=Blob.valueOf('Test Data'), ParentId=objTask2.Id);
        insert objAttachment;
    }
    
    //Using this function in :
    // 1. SL_TestMeetingNotesRelatedListController
    public static void createTestDataForMeetingNotes()
    {
		MN_UserOption__c objMN_UserOption = new MN_UserOption__c(Open_Activities__c='Test_OA', Closed_Activities__c='Test_CA', User__c=UserInfo.getUserId());
        insert objMN_UserOption;

        Account objAccount = new Account(Name='Test');
        insert objAccount;

        Contact objContact = new Contact(FirstName='Test', LastName='Contact',Email = 'test@test.com',AccountId = objAccount.Id);
        insert objContact;
 
        Task objTask = new Task();
        objTask.put('isParent__c', true); 
        objTask.WhatId=objAccount.Id;
        insert objTask;

        Task objTask2 = new Task();
        objTask2.put('isParent__c', false); 
        objTask2.WhatId=objAccount.Id;
        objTask2.put('ParentId__c' , objTask.Id);
        insert objTask2;

        Task objTask3 = new Task();
        objTask3.put('isParent__c', true);
        objTask3.WhatId=objAccount.Id;
        objTask3.Status='Completed'; 
        insert objTask3;

        Task objTask4 = new Task();
        objTask4.put('isParent__c', false);
        objTask4.WhatId=objAccount.Id;
        objTask4.Status='Completed'; 
        objTask4.put('ParentId__c', objTask3.Id); 
        insert objTask4;

        Event objEvent = new Event();
        objEvent.WhatId=objAccount.Id;
        objEvent.StartDateTime=Datetime.Now(); 
        objEvent.EndDateTime=Datetime.Now();
        objEvent.put('isParent__c', true);
        insert objEvent;

        Event objEvent2 = new Event();
        objEvent2.WhatId=objAccount.Id;
        objEvent2.StartDateTime=Datetime.Now();
        objEvent2.EndDateTime=Datetime.Now();
        objEvent2.put('ParentId__c' , objEvent.Id);
        objEvent2.put('isParent__c', false);
        insert objEvent2;

        Event objEvent3 = new Event();
        objEvent3.WhatId=objAccount.Id;
        objEvent3.StartDateTime=Datetime.Now().addMinutes(-10);
        objEvent3.EndDateTime=Datetime.Now();
        objEvent3.put('isParent__c' , true);
        insert objEvent3;

        Event objEvent4 = new Event();
        objEvent4.WhatId=objAccount.Id;
        objEvent4.StartDateTime=Datetime.Now().addMinutes(-8); 
        objEvent4.EndDateTime=Datetime.Now();
        objEvent4.put('ParentId__c', objEvent3.Id) ;
        objEvent4.put('isParent__c', false);
        insert objEvent4;

        Attachment objAttachmentTask = new Attachment(Name='Test Attachment', Body=Blob.valueOf('Test Data'), ParentId=objTask.Id);
        insert objAttachmentTask;

        Attachment objAttachmentEvent = new Attachment(Name='Test Attachment2', Body=Blob.valueOf('Test Data2'), ParentId=objEvent.Id);
        insert objAttachmentEvent;        
        
    }
    
    //Using this function in :
    // 1. 
    public static void createTestDataForMapping()
    {
        List<Field_Mapping__c> lstFM = new List<Field_Mapping__c>();
        Custom_Mapping__c objCustomMapping = new Custom_Mapping__c(SObject_Type__c = 'Account');
        /// insert Custom_Mapping__c record with SObject_Type__c as Opportunity
        insert objCustomMapping;
        RecordType objRecordType  = [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName = 'Direct_Association' AND (SobjectType = 'Object_Relationship__c' OR SobjectType = 'SL_Convert__Object_Relationship__c') limit 1];
        RecordType objRecordType1 = [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName = 'Related_List' AND (SobjectType = 'Object_Relationship__c' OR SobjectType = 'SL_Convert__Object_Relationship__c') limit 1];
        RecordType objRecordType2 = [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName = 'Reference' AND (SobjectType = 'Field_Mapping__c' OR SobjectType = 'SL_Convert__Field_Mapping__c') limit 1];
        RecordType objRecordType3 = [SELECT Id,DeveloperName FROM RecordType WHERE DeveloperName = 'Value' AND (SobjectType = 'Field_Mapping__c' OR SobjectType = 'SL_Convert__Field_Mapping__c') limit 1];
        
        Account objAccount = new Account(Name = 'Test Account');
        /// insert account
        insert objAccount; 
        
        Contact objContact = new Contact(LastName = 'Test Contact');
        /// insert account
        insert objContact; 
        
        Task objTask1 = new Task (WhatId = objAccount.Id,Subject = '1. Sales - In Store Demo',ActivityDate = date.today().addDays(-2),Description = 'test');
        insert objTask1;
        
        // Direct_Association record types 
        //Inserting object relationship record
        Object_Relationship__c objObjectRelationship = new Object_Relationship__c(Name  = 'Object Relationship 1', Order__c = 1, Custom_Mapping__c = objCustomMapping.Id
                                                         , RecordTypeId = objRecordType.Id, Context_Object_API__c = 'Account',
                                                         Target_Object_API__c = 'Opportunity',  Target_Relationship_Id__c = 'AccountId'
                                                         , Context_Relationship_Id__c = 'Id',Related_Record_Parent_Id__c = 'OpportunityId', 
                                                          Copy_Notes__c = true, Copy_Attachments__c = true, Map_Record_Type__c = true);
        insert objObjectRelationship;
        
        //Inserting field values
        Field_Mapping__c objFieldMapping1 = new Field_Mapping__c(Active__c = true , RecordTypeId = objRecordType2.Id,Context_Field_Name__c = 'Name',
                                                                Target_Field_Name__c = 'Name' , Object_Relationship__c = objObjectRelationship.Id);
        lstFM.add(objFieldMapping1);
        
        Field_Mapping__c objFieldMapping2 = new Field_Mapping__c(Active__c = true , RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'CloseDate' , Target_Value__c = string.valueOf(date.today()) ,Object_Relationship__c = objObjectRelationship.Id);
        lstFM.add(objFieldMapping2);
        
        Field_Mapping__c objFieldMapping3= new Field_Mapping__c(Active__c = true , RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'stageName' , Target_Value__c = 'Lost' ,Object_Relationship__c = objObjectRelationship.Id);
        lstFM.add(objFieldMapping3);
        
        Field_Mapping__c objFieldMapping11= new Field_Mapping__c(Active__c = true , RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'TotalOpportunityQuantity' , Target_Value__c = '100.00' ,Object_Relationship__c = objObjectRelationship.Id);
        lstFM.add(objFieldMapping11);
        
        Field_Mapping__c objFieldMapping12 = new Field_Mapping__c(Active__c = true ,RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'IsPrivate' , Target_Value__c = 'true' ,Object_Relationship__c = objObjectRelationship.Id);
        lstFM.add(objFieldMapping12);
        
        Field_Mapping__c objFieldMapping13 = new Field_Mapping__c(Active__c = true ,RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'Amount' , Target_Value__c = '500.00' ,Object_Relationship__c = objObjectRelationship.Id);
        lstFM.add(objFieldMapping13);
        
        Field_Mapping__c objFieldMapping14 = new Field_Mapping__c(Active__c = true ,RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'OwnerId' , Target_Value__c = UserInfo.getUserId() ,Object_Relationship__c = objObjectRelationship.Id);
        lstFM.add(objFieldMapping14);
        
        // Related_List record types 
        Object_Relationship__c objObjectRelationship1 = new Object_Relationship__c(Name  = 'Object Relationship 2', Order__c = 2, Custom_Mapping__c = objCustomMapping.Id
                                                         , RecordTypeId = objRecordType1.Id, Context_Object_API__c = 'Account',
                                                         Target_Object_API__c = 'Task',  Target_Relationship_Id__c = 'WhatId.AccountId',
                                                          Parent_Object_Relationship__c = objObjectRelationship.Id
                                                         , Context_Relationship_Id__c = 'Task',Related_Record_Parent_Id__c = 'WhatId',
                                                          Copy_Notes__c = false, Copy_Attachments__c = false);
        insert objObjectRelationship1;
        
        /*Field_Mapping__c objFieldMapping4 = new Field_Mapping__c(Active__c = true , Context_Field_Name__c = 'Subject',RecordTypeId = objRecordType2.Id,
                                                                Target_Field_Name__c = 'Subject' , Object_Relationship__c = objObjectRelationship1.Id);
        lstFM.add(objFieldMapping4);
        */
        Field_Mapping__c objFieldMapping5 = new Field_Mapping__c(Active__c = true ,RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'ActivityDate' , Target_Value__c = string.valueOf(date.today()) ,Object_Relationship__c = objObjectRelationship1.Id);
        lstFM.add(objFieldMapping5);
        
        Attachment attachment = new Attachment(Body = Blob.valueOf('This is a testing data for attachment body'), Name = 'Test Attachment', ParentId = objAccount.Id, ContentType = 'txt');
        insert attachment;
        Attachment attachment1 = new Attachment(Body = Blob.valueOf('This is a testing data for attachment body'), Name = 'Test Attachment', ParentId = objAccount.Id, ContentType = 'txt');
        insert attachment1;
        
        Note note = new Note(Title='Test Note', Body = 'Test Body ',ParentId = objAccount.Id);
        insert note;
        Note note1 = new Note(Title='Test Note', Body = 'Test Body ',ParentId = objAccount.Id);
        insert note1;
       
        // Direct_Association record types 
        Object_Relationship__c objObjectRelationship3 = new Object_Relationship__c(Name  = 'Object Relationship 4', Order__c = 4, Custom_Mapping__c = objCustomMapping.Id
                                                         , RecordTypeId = objRecordType.Id, Context_Object_API__c = 'Opportunity',
                                                         Target_Object_API__c = 'OpportunityContactRole',  Target_Parent_Lookup__c = 'OpportunityId'
                                                         , Context_Relationship_Id__c = 'Id',
                                                         Parent_Object_Relationship__c = objObjectRelationship.Id,
                                                          Copy_Notes__c = false, Copy_Attachments__c = false);
        insert objObjectRelationship3;
        
        Field_Mapping__c objFieldMapping6 = new Field_Mapping__c(Active__c = true ,RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'Role' , Target_Value__c = 'Test',Object_Relationship__c = objObjectRelationship3.Id);
        lstFM.add(objFieldMapping6);
        
        Field_Mapping__c objFieldMapping7 = new Field_Mapping__c(Active__c = true ,RecordTypeId = objRecordType3.Id,
                                                                Target_Field_Name__c = 'ContactId' , Target_Value__c = objContact.Id ,Object_Relationship__c = objObjectRelationship3.Id);
        lstFM.add(objFieldMapping7);
        

        insert lstFM;
    }
    
    public static void createTestDataforMNS1()
    {
    	Account objAcc = new Account(Name='Test Account');
		insert objAcc;

		Contact objCont = new Contact(LastName='Test Contact');
		insert objCont;

		Task objTask = new Task(Subject = 'Test Task', WhatId=objAcc.Id, WhoId=objCont.Id);
		insert objTask;

		Event objEvent = new Event(Subject = 'Test Event', StartDateTime = Datetime.now(), EndDateTime = Datetime.now(), WhatId=objAcc.Id, WhoId=objCont.Id);
		insert objEvent;


		Map<String, Schema.SObjectType> mapGlobalDescribe = Schema.getGlobalDescribe(); 
		
		SObject sObjMNPL1 = mapGlobalDescribe.get('MN_Page_Layout__c').newSObject();
		sObjMNPL1.put('Name', 'Master_Task');
		sObjMNPL1.put('Activity_Type__c', 'Task');
		sObjMNPL1.put('Additional_Buttons__c', 'Test');
		sObjMNPL1.put('Email_Template__c', 'Test');
		sObjMNPL1.put('FieldSetName__c', 'SL_TaskDescription');
		sObjMNPL1.put('Profile__c', 'System Administrator');
		sObjMNPL1.put('RecordType__c', 'Test');
		sObjMNPL1.put('Send_Email__c', true);
		sObjMNPL1.put('SendEmail_SOSL__c', 'Test');

        insert sObjMNPL1;

        SObject sObjMNPL2 = mapGlobalDescribe.get('MN_Page_Layout__c').newSObject();
		sObjMNPL2.put('Name', 'Master_Event');
		sObjMNPL2.put('Activity_Type__c', 'Event');
		sObjMNPL2.put('Additional_Buttons__c', 'Test');
		sObjMNPL2.put('Email_Template__c', 'Test');
		sObjMNPL2.put('FieldSetName__c', 'SL_EventDescription');
		sObjMNPL2.put('Profile__c', 'System Administrator');
		sObjMNPL2.put('RecordType__c', 'Test');
		sObjMNPL2.put('Send_Email__c', false);
		sObjMNPL2.put('SendEmail_SOSL__c', 'Test');
		
        insert sObjMNPL2; 

        SObject sObjMNRL1 = mapGlobalDescribe.get('MN_Related_List__c').newSObject();
        sObjMNRL1.put('Name', 'RL1');
		sObjMNRL1.put('DisplayName__c', 'Accounts');
		sObjMNRL1.put('ObjectName__c', 'Account');
		sObjMNRL1.put('Order__c', 1);
		sObjMNRL1.put('Record_Types_To_Be_Excluded__c', 'Test1');
		sObjMNRL1.put('RecordType__c', 'Test1, Test2');
		sObjMNRL1.put('FieldSetName__c', 'RL1FS');
		sObjMNRL1.put('Mobile_Fields__c', 'Name');
		sObjMNRL1.put('Rollup_API_Name__c', 'Test1, Test2');
		
        insert sObjMNRL1;

        SObject sObjMNRL2 = mapGlobalDescribe.get('MN_Related_List__c').newSObject();
        sObjMNRL2.put('Name', 'RL2');
		sObjMNRL2.put('DisplayName__c', 'Contacts');
		sObjMNRL2.put('ObjectName__c', 'Contact');
		sObjMNRL2.put('Order__c', 2);
		sObjMNRL2.put('Record_Types_To_Be_Excluded__c', 'Test1, Test2');
		sObjMNRL2.put('RecordType__c', 'Test1');
		sObjMNRL2.put('FieldSetName__c', 'RL2FS');
		sObjMNRL2.put('Mobile_Fields__c', 'Name');
		sObjMNRL2.put('Rollup_API_Name__c', 'Test1');
		
        insert sObjMNRL2;

        SObject sObjMNRL3 = mapGlobalDescribe.get('MN_Related_List__c').newSObject();
        sObjMNRL3.put('Name', 'RL3');
		sObjMNRL3.put('DisplayName__c', 'Cases');
		sObjMNRL3.put('ObjectName__c', 'Case');
		sObjMNRL3.put('Order__c', 3);
		sObjMNRL3.put('Record_Types_To_Be_Excluded__c', '');
		sObjMNRL3.put('RecordType__c', '');
		sObjMNRL3.put('FieldSetName__c', 'RL3FS');
		sObjMNRL3.put('Mobile_Fields__c', '');
		sObjMNRL3.put('Rollup_API_Name__c', '');
		
        insert sObjMNRL3;

        SObject sObjMNRL4 = mapGlobalDescribe.get('MN_Related_List__c').newSObject();
        sObjMNRL4.put('Name', 'RL4');
		sObjMNRL4.put('DisplayName__c', 'Contracts');
		sObjMNRL4.put('ObjectName__c', 'Contract');
		sObjMNRL4.put('Order__c', 4);
		sObjMNRL4.put('Record_Types_To_Be_Excluded__c', '');
		sObjMNRL4.put('RecordType__c', '');
		sObjMNRL4.put('FieldSetName__c', 'RL4FS');
		sObjMNRL4.put('Mobile_Fields__c', '');
		sObjMNRL4.put('Rollup_API_Name__c', '');
		
        insert sObjMNRL4;

        SObject sObjMNRL5 = mapGlobalDescribe.get('MN_Related_List__c').newSObject();
        sObjMNRL5.put('Name', 'RL5');
		sObjMNRL5.put('DisplayName__c', 'Solutions');
		sObjMNRL5.put('ObjectName__c', 'Solution');
		sObjMNRL5.put('Order__c', 5);
		sObjMNRL5.put('Record_Types_To_Be_Excluded__c', '');
		sObjMNRL5.put('RecordType__c', '');
		sObjMNRL5.put('FieldSetName__c', 'RL5FS');
		sObjMNRL5.put('Mobile_Fields__c', '');
		sObjMNRL5.put('Rollup_API_Name__c', '');
		
        insert sObjMNRL5;

        SObject sObjMNJ1 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ1.put('Name', 'PL1RL1');
		sObjMNJ1.put('MN_Page_Layout__c', sObjMNPL1.Id);
		sObjMNJ1.put('MN_Related_List__c', sObjMNRL1.Id);
		
        insert sObjMNJ1;

        SObject sObjMNJ2 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ2.put('Name', 'PL1RL2');
		sObjMNJ2.put('MN_Page_Layout__c', sObjMNPL1.Id);
		sObjMNJ2.put('MN_Related_List__c', sObjMNRL2.Id);
		
        insert sObjMNJ2;

        SObject sObjMNJ3 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ3.put('Name', 'PL1RL3');
		sObjMNJ3.put('MN_Page_Layout__c', sObjMNPL1.Id);
		sObjMNJ3.put('MN_Related_List__c', sObjMNRL3.Id);
		
        insert sObjMNJ3;

        SObject sObjMNJ4 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ4.put('Name', 'PL1RL4');
		sObjMNJ4.put('MN_Page_Layout__c', sObjMNPL1.Id);
		sObjMNJ4.put('MN_Related_List__c', sObjMNRL4.Id);
		
        insert sObjMNJ4;

        SObject sObjMNJ5 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ5.put('Name', 'PL1RL5');
		sObjMNJ5.put('MN_Page_Layout__c', sObjMNPL1.Id);
		sObjMNJ5.put('MN_Related_List__c', sObjMNRL5.Id);
		
        insert sObjMNJ5;

        SObject sObjMNJ6 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ6.put('Name', 'PL2RL1');
		sObjMNJ6.put('MN_Page_Layout__c', sObjMNPL2.Id);
		sObjMNJ6.put('MN_Related_List__c', sObjMNRL1.Id);
		
        insert sObjMNJ6;

        SObject sObjMNJ7 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ7.put('Name', 'PL2RL2');
		sObjMNJ7.put('MN_Page_Layout__c', sObjMNPL2.Id);
		sObjMNJ7.put('MN_Related_List__c', sObjMNRL2.Id);
		
        insert sObjMNJ7;

        SObject sObjMNJ8 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ8.put('Name', 'PL2RL3');
		sObjMNJ8.put('MN_Page_Layout__c', sObjMNPL2.Id);
		sObjMNJ8.put('MN_Related_List__c', sObjMNRL3.Id);
		
        insert sObjMNJ8;

        SObject sObjMNJ9 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ9.put('Name', 'PL2RL4');
		sObjMNJ9.put('MN_Page_Layout__c', sObjMNPL2.Id);
		sObjMNJ9.put('MN_Related_List__c', sObjMNRL4.Id);
		
        insert sObjMNJ9;

        SObject sObjMNJ10 = mapGlobalDescribe.get('MN_Joiner__c').newSObject();
        sObjMNJ10.put('Name', 'PL2RL5');
		sObjMNJ10.put('MN_Page_Layout__c', sObjMNPL2.Id);
		sObjMNJ10.put('MN_Related_List__c', sObjMNRL5.Id);
		
        insert sObjMNJ10;
    } 
    
    
    
    
    
    
    
    
    
    
    
    public static User createTestUser(Id profID, String fName, String lName) {
        String orgId = userInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User objTestuser = new User(  firstname = fName,
                            lastName = lName,
                            email = uniqueName + '@test' + orgId + '.org',
                            Username = uniqueName + '@test' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            isActive = true,
                            ProfileId = profId);
        return objTestuser;
    }

    public static SObject createSObject(SObject sObj) {
        // Check what type of object we are creating and add any defaults that are needed.
        String objectName = String.valueOf(sObj.getSObjectType());
        // Construct the default values class. Salesforce doesn't allow '__' in class names
        String defaultClassName = 'SL_TestDataFactory.' + objectName.replaceAll('__(c|C)$|__', '') + 'Defaults';
        // If there is a class that exists for the default values, then use them
        if (Type.forName(defaultClassName) != null) {
            sObj = createSObject(sObj, defaultClassName);
        }
        return sObj;
    }

    public static SObject createSObject(SObject sObj, Boolean doInsert) {
        SObject retObject = createSObject(sObj);
        if (doInsert) {
            insert retObject;
        }
        return retObject;
    }

    public static SObject createSObject(SObject sObj, String defaultClassName) {
        // Create an instance of the defaults class so we can get the Map of field defaults
        Type t = Type.forName(defaultClassName);
        if (t == null) {
            Throw new TestFactoryException('Invalid defaults class.');
        }
        FieldDefaults defaults = (FieldDefaults)t.newInstance();
        addFieldDefaults(sObj, defaults.getFieldDefaults());
        return sObj;
    }

    public static SObject createSObject(SObject sObj, String defaultClassName, Boolean doInsert) {
        SObject retObject = createSObject(sObj, defaultClassName);
        if (doInsert) {
            insert retObject;
        }
        return retObject;
    }

    public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects) {
        return createSObjectList(sObj, numberOfObjects, (String)null);
    }

    public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, Boolean doInsert) {
        SObject[] retList = createSObjectList(sObj, numberOfObjects, (String)null);
        if (doInsert) {
            insert retList;
        }
        return retList;
    }

    public static SObject[] createSObjectList(SObject sObj, Integer numberOfObjects, String defaultClassName, Boolean doInsert) {
        SObject[] retList = createSObjectList(sObj, numberOfObjects, defaultClassName);
        if (doInsert) {
            insert retList;
        }
        return retList;
    }

    public static SObject[] createSObjectList(Sobject sObj, Integer numberOfObjects, String defaultClassName) {
        SObject[] sObjs = new SObject[] {};
        SObject newObj;

        // Get one copy of the object
        if (defaultClassName == null) {
            newObj = createSObject(sObj);
        } else {
            newObj = createSObject(sObj, defaultClassName);
        }

        // Get the name field for the object
        String nameField = nameFieldMap.get(String.valueOf(sObj.getSObjectType()));
        if (nameField == null) {
            if(String.valueOf(sObj.getSObjectType()) != 'LiveChatTranscript')
                nameField = 'Name';
        }

        // Clone the object the number of times requested. Increment the name field so each record is unique
        for (Integer i = 0; i < numberOfObjects; i++) {
            SObject clonedSObj = newObj.clone(false, true);
            if(nameField != NULL)
                clonedSObj.put(nameField, (String)clonedSObj.get(nameField) + ' ' + i);
            
            if(String.valueOf(sObj.getSObjectType()) == 'Quote__c')
                clonedSObj.put('Quote_EDW_ID__c', (String)clonedSObj.get('Quote_EDW_ID__c') + '' + i);
                    
            sObjs.add(clonedSObj);
        }
        return sObjs;
    }

    private static void addFieldDefaults(SObject sObj, Map<Schema.SObjectField, Object> defaults) {
        // Loop through the map of fields and if they are null on the object, fill them.
        for (Schema.SObjectField field : defaults.keySet()) {
            if (sObj.get(field) == null) {
                sObj.put(field, defaults.get(field));
            }
        }
    }

    // When we create a list of SObjects, we need to
    private static Map<String, String> nameFieldMap = new Map<String, String> {
        'Contact' => 'LastName',
        'Case' => 'Subject',
        'Event' => 'Subject',
        'Task' => 'Subject'
    };

    //used to soft assert primitive data types
    public static void softAssertEquals(Object expectedResult, Object actualResult, String errorMessage){
        String message;

        if(expectedResult == actualResult) {
            passes++;
            message = '    Soft Assert Passed: [ Expected ' + expectedResult + ' = Actual ' + actualResult + ']\n';
            system.debug(message);
            trackSoftAssertResult(true, message);
        }
        else {
            failures++;
            message = '    Soft Assert Failed: [ Expected ' + expectedResult + ' != Actual ' + actualResult + ']';
            message += String.isNotBlank(errorMessage) ? ' ' + errorMessage + '\n' : '\n';
            system.debug(message);
            trackSoftAssertResult(false, message);
        }
    }
    public static void softAssertEquals(Object expectedResult, Object actualResult){
        softAssertEquals(expectedResult, actualResult, null);
    }
    //used to soft assert primitive data types
    public static void softAssertNotEquals(Object expectedResult, Object actualResult, String errorMessage){
        String message;

        if(expectedResult == actualResult) {
            failures++;
            message = '    Soft Assert Failed: [Expected ' + expectedResult + ' =  Actual ' + actualResult + ']';
            message += String.isNotBlank(errorMessage) ? ' ' + errorMessage + '\n' : '\n';

            system.debug(message);
            trackSoftAssertResult(false, message);
        }
        else {
            passes++;
            message = '    Soft Assert Passed: [Expected ' + expectedResult + ' !=  Actual ' + actualResult + ']\n';
            system.debug(message);
            trackSoftAssertResult(true, message);
        }
    }

    public static void softAssertNotEquals(Object expectedResult, Object actualResult){
        softAssertNotEquals(expectedResult, actualResult, null);
    }

    public static void softAssert(Boolean result, String errorMessage) {
        if (result) {
            passes++;

            trackSoftAssertResult(true, '    Soft Assert Passed ');
        } else {
            failures++;
            String message = '    Soft Assert Failed ';
            message += String.isNotBlank(errorMessage) ? ': ' + errorMessage + '\n' : '\n';

            system.debug(errorMessage);
            trackSoftAssertResult(false, message);
        }
    }

    public static void softAssert(Boolean result) {
        softAssert(result, '');
    }
    
    //call after all soft asserts completed in a given method to determine
    // if any or all test passed
    // if one or more soft assert test fail, entire method will fail
    public static void hardAssertAllResults() {
        if (mapMessageByResult.containsKey(false)) {
            //fail
            String failResult = mapMessageByResult.get(false);
            String passResult = mapMessageByResult.containsKey(true) ? ' with results ' + mapMessageByResult.get(true) : '';
            mapMessageByResult.clear(); //clear results for next test
            System.assert(false, 'Fail: ' + failures + ' test(s) fail because of \n' + failResult + ' '
                         + passes + ' test(s) passes' + passResult);
        } else {
            //pass
            System.assert(true);
        }
        mapMessageByResult.clear();
    }

    private static void trackSoftAssertResult(Boolean hasPass, String testResult) {
        if(mapMessageByResult.containsKey(hasPass)) {
            testResult = mapMessageByResult.get(hasPass)  + testResult;
        }
        mapMessageByResult.put(hasPass, testResult);
    }

    public class TestFactoryException extends Exception {}

    // Use the FieldDefaults interface to set up values you want to default in for all objects.
    public interface FieldDefaults {
        Map<Schema.SObjectField, Object> getFieldDefaults();
    }

    // To specify defaults for objects, use the naming convention [ObjectName]Defaults.
    // For custom objects, omit the __c from the Object Name

    public class AccountDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Account.Name => 'Test Account'
            };
        }
    }

    public class ContactDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Contact.FirstName => 'First',
                Contact.LastName => 'Last'
            };
        }
    }

    public class OpportunityDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Opportunity.Name => 'Test Opportunity',
                Opportunity.StageName => 'Closed Won',
                Opportunity.CloseDate => System.today()
            };
        }
    }

    public class CaseDefaults implements FieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                Case.Subject => 'Test Case',
                Case.Origin => 'Email'
            };
        }
    }
    
    public class UserDefaults implements FieldDefaults {

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    User.LastName => 'Member Services',
                    User.Email => 'testUser@test.com',
                    User.CompanyName => 'PURE',
                    User.ProfileId => SL_TestDataFactory.strUserProfileId,
                    User.Title => 'Member Services',
                    User.Alias => 'mser',
                    User.TimeZoneSidKey => 'America/Chicago',
                    User.EmailEncodingKey => 'UTF-8',
                    User.LanguageLocaleKey => 'en_US',
                    User.LocaleSidKey => 'en_US',
                    User.UserRoleId => SL_TestDataFactory.strUserRoleId,
                    User.Username => SL_TestDataFactory.strUserName
            };
        }
    }


    private class TestDataFactoryException extends Exception{}

}