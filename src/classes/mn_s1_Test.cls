@isTest
private class mn_s1_Test {
    
    @isTest 
    static void unitTestMN_S1() {

        SL_TestDataFactory.createTestDataforMNS1();
        
        Task objTask = [Select Id from Task limit 1];
        Event objEvent = [Select Id from Event limit 1];

        PageReference PageRefTask = Page.mn_s1_task;
        Test.setCurrentPage(PageRefTask);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objTask);
        ApexPages.currentPage().getParameters().put('id', objTask.Id);
        
        mn_s1 objMNS1 = new mn_s1(sc);

        String strTaskResponse = mn_s1.init('System Administrator','Task','','',objTask.Id);
        
        system.assert(strTaskResponse != NULL); 

        String strEventResponse = mn_s1.init('System Administrator','Event','','',objEvent.Id);
        
        system.assert(strEventResponse != NULL);
    }
    
}