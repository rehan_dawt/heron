global without sharing class SL_MeetingNotesRelatedListExtension {
    
    global SL_MeetingNotesRelatedListExtension(SL_MeetingNotesRelatedListController controller) { }

    @remoteAction
    global static String queryWithoutSharing(String soql) {
        
        DateTime dt = datetime.Now();
        List<sObject> records;
        List<UserRecordAccess> lstRecordAccess = new List<UserRecordAccess>();
        List<sObject> accessibleRecords = new List<sObject>();

        try {

            if(soql.contains('PLACEHOLDER-OPENACTIVITY')) {

                soql = soql.replace('PLACEHOLDER-OPENACTIVITY', 'AND StartDateTime >: dt');                
                records = Database.query(soql);   

            } else if(soql.contains('PLACEHOLDER-CLOSEDACTIVITY')) {

                soql = soql.replace('PLACEHOLDER-CLOSEDACTIVITY', 'AND StartDateTime <: dt');
                records = Database.query(soql);   

            } else {

                records = Database.query(soql);    
            }

            if(!records.isEmpty()) {

                List<Id> lstActivityIds = new List<Id>();
                Set<Id> setReadAccessRecordIds = new Set<Id>();
                List<List<Id>> lstActivityIdChunks = new List<List<Id>>();
                Integer count=0;

                for(sObject obj:records) {

                    count++;

                    lstActivityIds.add((Id)obj.get('Id'));

                    if(lstActivityIds.size() == 200) {

                        lstActivityIdChunks.add(lstActivityIds);
                        lstActivityIds = new List<Id>(); 

                    } else if(count == records.size()) {

                        lstActivityIdChunks.add(lstActivityIds);
                    }
                }

                if(!lstActivityIdChunks.isEmpty()) {

                    for(Integer index=0;index<lstActivityIdChunks.size();index++) {

                        lstRecordAccess.addAll([SELECT RecordId FROM UserRecordAccess WHERE UserId =: UserInfo.getUserId() AND RecordId IN:lstActivityIdChunks[index] AND HasReadAccess=true]);
                    }
                }

                for(UserRecordAccess obj:lstRecordAccess) {

                    setReadAccessRecordIds.add(obj.RecordId);
                }

                for(sObject obj:records) {

                    if(setReadAccessRecordIds.contains((Id)obj.get('Id')))
                        accessibleRecords.add(obj);
                }
            }

        } catch (QueryException qe) {
            return '[{"message":"'+qe.getMessage()+'","errorCode":"INVALID_QUERY"}]';
        }
        
        Map<String, Object> result = new Map<String, Object>();
        result.put('records', accessibleRecords);
        result.put('totalSize', accessibleRecords.size());
        result.put('done', true);
        
        return JSON.serialize(result);
    }


    @remoteAction
    global static String queryOnViewAllModeWithoutSharing(String soql, Integer offset, Integer startOffset) {
        
        DateTime dt = datetime.Now();
        List<sObject> records;
        List<UserRecordAccess> lstRecordAccess = new List<UserRecordAccess>();
        List<sObject> accessibleRecords = new List<sObject>();
        Integer chunkSizeOnViewAll;
        if(Test.isRunningTest())
            chunkSizeOnViewAll = 500;
        else 
            chunkSizeOnViewAll = integer.valueOf(SL_MN_Related_List__c.getInstance().Chunk_Size_On_ViewAll__c);
        try {

            if(soql.contains('PLACEHOLDER-OPENACTIVITY')) {

                soql = soql.replace('PLACEHOLDER-OPENACTIVITY', 'AND StartDateTime >: dt');                
                records = Database.query(soql);   

            } else if(soql.contains('PLACEHOLDER-CLOSEDACTIVITY')) {

                soql = soql.replace('PLACEHOLDER-CLOSEDACTIVITY', 'AND StartDateTime <: dt');
                records = Database.query(soql);   

            } else {

                records = Database.query(soql);    
            }

            if(!records.isEmpty()) {

                List<Id> lstActivityIds = new List<Id>();
                Set<Id> setReadAccessRecordIds = new Set<Id>();
                List<List<Id>> lstActivityIdChunks = new List<List<Id>>();
                Integer count=0;

                if(chunkSizeOnViewAll+offset > records.size())
                    offset = records.size();
                else
                    offset = chunkSizeOnViewAll+offset;

                for(Integer i=startOffset; i<offset; i++){
                    
                    count++;

                    lstActivityIds.add(records[i].Id);

                    if(lstActivityIds.size() == 200) {

                        lstActivityIdChunks.add(lstActivityIds);
                        lstActivityIds = new List<Id>(); 

                    } else if(count == (offset-startOffset)) {

                        lstActivityIdChunks.add(lstActivityIds);
                    }
                }

                if(!lstActivityIdChunks.isEmpty()) {

                    for(Integer index=0;index<lstActivityIdChunks.size();index++) {

                        lstRecordAccess.addAll([SELECT RecordId FROM UserRecordAccess WHERE UserId =: UserInfo.getUserId() AND RecordId IN:lstActivityIdChunks[index] AND HasReadAccess=true]);
                    }
                }

                for(UserRecordAccess obj:lstRecordAccess) {

                    setReadAccessRecordIds.add(obj.RecordId);
                }

                for(sObject obj:records) {

                    if(setReadAccessRecordIds.contains((Id)obj.get('Id')))
                        accessibleRecords.add(obj);
                }
            }

        } catch (QueryException qe) {
            return '[{"message":"'+qe.getMessage()+'","errorCode":"INVALID_QUERY"}]';
        }
        
        Map<String, Object> result = new Map<String, Object>();
        result.put('records', accessibleRecords);
        result.put('totalSize', accessibleRecords.size());
        result.put('done', true);
        
        return JSON.serialize(result);
    }
}