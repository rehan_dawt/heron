public with sharing class SL_MappingValidationUtils {
	// should accept a list/map of Object relationships and return a list/map of Object relationships with added errors
	// should accept a list/map of Field mappings and return a list/map of Field mappings with added errors
	// maybe it should cascade down, and you can pass in a custom mapping and it'll do full validations ultimately
	public static Map<String, Schema.SObjectType> allSObjects;
	public static Id referenceRecordTypeId;

	public static void customMappingValidate (Map <Id, Custom_Mapping__c> customMappings){
		customMappingValidate(customMappings.values());
	}	

	public static void customMappingValidate (List <Custom_Mapping__c> customMappings){
		if (allSObjects==null){
			allSObjects = Schema.getGlobalDescribe();
		}
		for (Custom_Mapping__c customMapping: customMappings){
			if (customMapping.Active__c){
				if(!allSObjects.containsKey(customMapping.SObject_Type__c.toLowerCase())){
					customMapping.addError('No such sObject!');
				}
			}
		}
	}	

	public static void objectRelationshipValidate (Map <Id, Object_Relationship__c> objectRelationships){
		objectRelationshipValidate(objectRelationships.values());
	}
	// assumes the list is of object relationships related to a single custom mapping
	public static void objectRelationshipValidate (List<Object_Relationship__c> objectRelationships){
		if (allSObjects==null){
			allSObjects = Schema.getGlobalDescribe();
		}

		Map<Id, List<Object_Relationship__c>> customMappingToObjectRelationshipsForValidation = new Map<Id, List<Object_Relationship__c>>();

		// build a map from custom mapping to object relationships
		for (Object_Relationship__c objectRelationship: objectRelationships){
			Id customMappingId = objectRelationship.Custom_Mapping__c;
			if (customMappingToObjectRelationshipsForValidation.containsKey(customMappingId)){
				customMappingToObjectRelationshipsForValidation.get(customMappingId).add(objectRelationship);
			}
			else {
				customMappingToObjectRelationshipsForValidation.put(customMappingId, new List<Object_Relationship__c>{objectRelationship});
			}
		}
		// for each custom mapping, test their object relationships
		for (Custom_Mapping__c customMapping: 
			[SELECT
				Id, 
				(SELECT
					Active__c,
					Context_Object_API__c,
					Target_Object_API__c,
					Context_Relationship_Id__c,
					Order__c,
					Parent_Object_Relationship__c,
					Related_Record_Parent_Id__c,
					Target_Relationship_Id__c,
					RecordType.DeveloperName
				FROM Object_Relationships__r)
			FROM Custom_Mapping__c
			WHERE Id IN :customMappingToObjectRelationshipsForValidation.keySet()]){
			validateObjectRelationships(customMappingToObjectRelationshipsForValidation.get(customMapping.id), customMapping.Object_Relationships__r);
		}
	}

	public static void validateObjectRelationships(List<Object_Relationship__c> objectRelationshipsToValidate, List<Object_Relationship__c> objectRelationshipsForCustomMapping){
		if (allSObjects==null){
			allSObjects = Schema.getGlobalDescribe();
		}
		if (objectRelationshipsToValidate.isEmpty()){
			return;
		}

		Set<Id> validateIds = new Set<Id>();
		for (Object_Relationship__c objectRelationship: objectRelationshipsToValidate){
			validateIds.add(objectRelationship.Id);
		}

		Set<Integer> orderNumbers = new Set<Integer>();
		for (Object_Relationship__c objectRelationship: objectRelationshipsForCustomMapping){
			if (!validateIds.contains(objectRelationship.Id)){
				orderNumbers.add((Integer) objectRelationship.Order__c);
			}
		}

		for (Object_Relationship__c objectRelationship: objectRelationshipsToValidate){
			validateObjectRelationship(objectRelationship, orderNumbers);		 
		}
	}

	public static void validateObjectRelationship(Object_Relationship__c objectRelationship, Set<Integer> orderNumbers) {
		if (allSObjects==null){
			allSObjects = Schema.getGlobalDescribe();
		}
		if (objectRelationship.Active__c){
			// validate order uniqueness
			if (orderNumbers.contains((Integer)objectRelationship.Order__c)){
				objectRelationship.addError('Duplicate Order number');
			}
			if (objectRelationship.Context_Object_API__c==null){
				objectRelationship.addError('No context object is specified!');
			}
			else {
				// validate context object exists
				if (!allSObjects.containsKey(objectRelationship.Context_Object_API__c.toLowerCase())){
					objectRelationship.addError('No such context object ' + objectRelationship.Context_Object_API__c + ' exists!');
				}
			}
			if (objectRelationship.Target_Object_API__c==null){
				objectRelationship.addError('No target object is specified!');
			}
			else {
				// validate target object exists
				if (!allSObjects.containsKey(objectRelationship.Target_Object_API__c.toLowerCase())){
					objectRelationship.addError('No such target object ' + objectRelationship.Target_Object_API__c + ' exists!');
				}
			}
			
			if (objectRelationship.RecordType.DeveloperName=='Direct_Association'){
				if (objectRelationship.Context_Relationship_Id__c.toLowerCase()!='id'){
					objectRelationship.addError('Context relationship id is invalid, must be "id"!');
				}
				if (objectRelationship.Target_Object_API__c != null
					&& allSObjects.containsKey(objectRelationship.Target_Object_API__c.toLowerCase())
					&& !allSObjects.get(objectRelationship.Target_Object_API__c.toLowerCase()).getDescribe().fields.getMap().containsKey(objectRelationship.Target_Relationship_Id__c.toLowerCase())){
					objectRelationship.addError('No such lookup field ' + objectRelationship.Target_Relationship_Id__c + ' on ' + objectRelationship.Target_Object_API__c + ' exists!');
				}
				
			}
			else if (objectRelationship.RecordType.DeveloperName=='Related_List'){
				if (!allSObjects.containsKey(objectRelationship.Context_Relationship_Id__c.toLowerCase())){
					objectRelationship.addError('Context relationship id is invalid, no such object ' + objectRelationship.Context_Relationship_Id__c + ' exists!');
				}
				if (objectRelationship.Parent_Object_Relationship__c==null){
					objectRelationship.addError('Related list relationship without parent relationship!');
				}
				if (objectRelationship.Target_Object_API__c != null
					&&  allSObjects.containsKey(objectRelationship.Target_Object_API__c.toLowerCase())
					&&  !allSObjects.get(objectRelationship.Target_Object_API__c.toLowerCase()).getDescribe().fields.getMap().containsKey(objectRelationship.Related_Record_Parent_Id__c.toLowerCase())) {
					objectRelationship.addError('No such lookup field ' + objectRelationship.Related_Record_Parent_Id__c + ' on ' + objectRelationship.Target_Object_API__c + ' exists!');
				}
			}
			else {
				System.debug(LoggingLevel.ERROR, 'ERROR: OBJECT RELATIONSHIP HAS NO RECORD TYPE.');
			}
		}
	}

	public static void fieldMappingValidate (Map <Id, Field_Mapping__c> fieldMappings){
		fieldMappingValidate(fieldMappings.values());
	}
	// accepts a list of field mappings, tests them in the context of their object relationships
	public static void fieldMappingValidate (List <Field_Mapping__c> fieldMappings){
		if (fieldMappings.isEmpty()){
			return;
		}
		Map<Id, List<Field_Mapping__c>> objectRelationshipToFieldMappingsForValidation = new Map<Id, List<Field_Mapping__c>>();

		// build a map from object relationship to field mappings
		for (Field_Mapping__c fieldMapping: fieldMappings){
			Id objRelId = fieldMapping.Object_Relationship__c;
			if (objectRelationshipToFieldMappingsForValidation.containsKey(objRelId)){
				objectRelationshipToFieldMappingsForValidation.get(objRelId).add(fieldMapping);
			}
			else {
				objectRelationshipToFieldMappingsForValidation.put(objRelId, new List<Field_Mapping__c>{fieldMapping});
			}
		}
		// for each object relationship, test their field mappings
		for (Object_Relationship__c objectRelationship: 
			[SELECT
				Id, 
				Target_Relationship_Id__c,
				Target_Parent_Lookup__c,
				(SELECT
					Context_Field_Name__c,
					Target_Field_Name__c,
					Context_Full_API__c,
					Target_Full_API__c,
					RecordType.DeveloperName,
					Active__c 
				FROM Field_Mappings__r)
			FROM Object_Relationship__c
			WHERE Id IN :objectRelationshipToFieldMappingsForValidation.keySet()]){
			validateFieldMappings(objectRelationshipToFieldMappingsForValidation.get(objectRelationship.id), objectRelationship.Field_Mappings__r, objectRelationship.Target_Relationship_Id__c, objectRelationship.Target_Parent_Lookup__c);
		}
	}
	// assumes a single object relationship's list of field mappings
	public static void validateFieldMappings(List <Field_Mapping__c> fieldMappingsToValidate, List <Field_Mapping__c> fieldMappingsForObjectRel, String relTargetRelationshipId, String relTargetParentLookup){
		if (allSObjects==null){
			allSObjects = Schema.getGlobalDescribe();
		}
		if (fieldMappingsToValidate.isEmpty()){
			return;
		}
		String contextObjectAPI = getContextObjectAPI(fieldMappingsToValidate);
		String targetObjectAPI;

		if (fieldMappingsToValidate[0].Target_Full_API__c.substringBefore('.').endsWith('__r')){
			targetObjectAPI=fieldMappingsToValidate[0].Target_Full_API__c.substringBefore('.').replace('__r', '__c');
		}
		else {
			targetObjectAPI=fieldMappingsToValidate[0].Target_Full_API__c.substringBefore('.');
		}
		Set<String> possibleContextFields = getFieldAPINames(contextObjectAPI);
		Set<String> possibleTargetFields = getFieldAPINames(targetObjectAPI);

        Set<Id> validateIds = new Set<Id>();
		for (Field_Mapping__c fieldMapping: fieldMappingsToValidate){
			validateIds.add(fieldMapping.Id);
		}

		Set<String> contextFields = new Set<String>();
		Set<String> targetFields = new Set<String>();
		for (Field_Mapping__c fieldMapping: fieldMappingsForObjectRel){
			if (!validateIds.contains(fieldMapping.Id)){
				if (fieldMapping.RecordType.DeveloperName=='Reference'){
					contextFields.add(fieldMapping.Context_Field_Name__c.toLowerCase());
				}
				targetFields.add(fieldMapping.Target_Field_Name__c.toLowerCase());
			}
		}

		if (referenceRecordTypeId==null){
			referenceRecordTypeId=[SELECT id FROM RecordType WHERE DeveloperName='Reference' AND (SObjectType='Field_Mapping__c' OR SObjectType='SL_Convert__Field_Mapping__c')].Id;
		}

		relTargetRelationshipId = relTargetRelationshipId != null ? relTargetRelationshipId.toLowerCase() : null;		
		relTargetParentLookup = relTargetParentLookup != null ? relTargetParentLookup.toLowerCase() : null;	

		for (Field_Mapping__c fieldMapping: fieldMappingsToValidate){
			// skip these validations if the field mapping is inactive
			if (fieldMapping.Active__c){
				if (fieldMapping.RecordTypeId==referenceRecordTypeId){
					if (fieldMapping.Context_Field_Name__c==null){
						fieldMapping.addError('No source field specified for target '+ fieldMapping.Target_Field_Name__c +'!');
					}
					else {
						// validate that the source field exists
						if (!possibleContextFields.contains(fieldMapping.Context_Field_Name__c.toLowerCase())){
							//fieldMapping.addError('No such source field exists!');
						}
					}
				}
				if (fieldMapping.Target_Field_Name__c==null){
						fieldMapping.addError('No target field specified!');
				}
				else {
					// validate that the target field exists
					if (!possibleTargetFields.contains(fieldMapping.Target_Field_Name__c.toLowerCase())){
						fieldMapping.addError('No such target field exists!');
					}
					// validate that the target field isn't a duplicate 
					if (targetFields.contains(fieldMapping.Target_Field_Name__c.toLowerCase())){
						fieldMapping.addError('Duplicate target field!');
					}
					if (relTargetRelationshipId == fieldMapping.Target_Field_Name__c.toLowerCase()){
						fieldMapping.addError('Attempting to map Target Relationship ID field!');
					}
					if (relTargetParentLookup == fieldMapping.Target_Field_Name__c.toLowerCase()){
						fieldMapping.addError('Attempting to map Target Parent Lookup field!');
					}
				}
			}
		}
	}

	private static String getContextObjectAPI(List<Field_Mapping__c> fieldMappings){
		for (Field_Mapping__c fieldMapping: fieldMappings){
			if (fieldMapping.Context_Full_API__c!=null){
				if (fieldMapping.Context_Full_API__c.substringBefore('.').endsWith('__r')){
					return fieldMapping.Context_Full_API__c.substringBefore('.').replace('__r', '__c');
				}
				else {
					return fieldMapping.Context_Full_API__c.substringBefore('.');
				}
			}
		}
		return null;
	}

	private static Set<String> getFieldAPINames(String SObjectToDescribe){
		Set<String> allFields = new Set<String>();
		if (SObjectToDescribe == null){
			return allFields;
		}
		List<SObjectField> describeContextFields = allSObjects.get(SObjectToDescribe).getDescribe().fields.getMap().values();

        for (SObjectField describeField: describeContextFields){
            allFields.add(describeField.getDescribe().getName().toLowerCase());
        } 

        return allFields;
	}
}