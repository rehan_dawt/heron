/*
    JIRA Ticket   : LIB-184
    Created on    : May-19-2014
    Created by    : Praful
    Description   : Trigger handler for SL_Event and SL_Task, and purpose is to delete the child activities on deletion of parent activity.
*/
public with sharing class SL_ActivityHandler {

    //Static variable to maintain recursion handler, as we are firing trigger on update and again performing update operation for tasks records
    public static boolean run = true;
    public Boolean closeRelatedActivities;

    public SL_ActivityHandler() {

        //To avoid addition of common dependencies while Package building
        String userQueryString = 'SELECT Close_Related_Activities__c FROM SL_MN_Related_List__c WHERE SetupOwnerId =\''+ UserInfo.getUserId()+'\' LIMIT 1';
        String proQueryString = 'SELECT Close_Related_Activities__c FROM SL_MN_Related_List__c WHERE SetupOwnerId=\''+ UserInfo.getProfileId()+'\' LIMIT 1';
        String orgQueryString = 'SELECT Close_Related_Activities__c FROM SL_MN_Related_List__c WHERE SetupOwnerId =\''+ UserInfo.getOrganizationId()+'\' LIMIT 1' ;

        List<sObject> userResult = Database.query(userQueryString);

        if(userResult.isEmpty()) {

            List<sObject> profileResult = Database.query(proQueryString);

            if(profileResult.isEmpty()) {
                
                List<sObject> orgResult = Database.query(orgQueryString);
                if(orgResult.isEmpty()) {

                    closeRelatedActivities = false;

                } else{

                    closeRelatedActivities = (Boolean)orgResult[0].get('Close_Related_Activities__c');
                }

            } else {

                closeRelatedActivities = (Boolean)profileResult[0].get('Close_Related_Activities__c');
            }

        } else {

            closeRelatedActivities = (Boolean)userResult[0].get('Close_Related_Activities__c');
        }
    }
    
    public void onAfterDelete(List<sObject> lstActivity) {
        
        Set<Id> setActivitiesIds = new Set<Id>(); // To hold the set of parent activities Ids
        List<sObject> lstChildActivitiesToDelete = new List<sObject>(); // To hold the list of child activities to delete

        // Iterating over list of activity
        for(sObject obj : lstActivity)
        {
            // Checking parent activity and collecting in set of activities ids
            if(obj.get('IsParent__c') == true)
                setActivitiesIds.add((Id)obj.get('Id'));
        }

        for(Event objEvent : [SELECT Id FROM Event WHERE ParentId__c IN: setActivitiesIds]){
            lstChildActivitiesToDelete.add(objEvent);
        } 

        for(Task objTask : [SELECT Id FROM Task WHERE ParentId__c IN: setActivitiesIds]) {        
            lstChildActivitiesToDelete.add(objTask);
        }

        if(!lstChildActivitiesToDelete.isEmpty()) {
            delete lstChildActivitiesToDelete;
        } 
    }

    //This method will execute on after update to update the related activities details
    public void onAfterUpdate(Map<Id, sObject> mapActivityNew, Map<Id, sObject> mapActivityOld) {
        
        set<Id> setActivityIds = new set<Id>();
        Map<Id,sObject> mapParentId_Activity = new Map<Id,sObject>();
        
        Schema.SObjectType objType;
        String dynamicQuery;
        List<sObject> lstActivityToUpdate = new List<sObject>();

        //Iterate over the updated activity records and collect them into map
        for(sObject objActivity: mapActivityNew.values()) {

            if(objActivity.get('isParent__c') == true && objActivity.get('ParentId__c') == null) {

                setActivityIds.add(objActivity.Id);
                mapParentId_Activity.put(objActivity.Id, objActivity);

            } else if(objActivity.get('isParent__c') == false && objActivity.get('ParentId__c') != null) {

                setActivityIds.add((Id)objActivity.get('ParentId__c'));
                mapParentId_Activity.put((Id)objActivity.get('ParentId__c'), objActivity);
            }            
        }

        if(!setActivityIds.isEmpty()) {

            objType = (new list<Id>(setActivityIds))[0].getSobjectType();

            if(objType == Task.SObjectType) {

                dynamicQuery = 'SELECT Id, Subject, isParent__c, ParentId__c, WhoId, WhatId, Status, IsReminderSet FROM Task WHERE ParentId__c IN: setActivityIds OR Id IN: setActivityIds';

            } else {

                dynamicQuery = 'SELECT Id, Subject, isParent__c, ParentId__c, WhoId, WhatId, IsReminderSet FROM Event WHERE ParentId__c IN: setActivityIds OR Id IN: setActivityIds';
            }

            for(sObject objActivity: Database.query(dynamicQuery)) {

                if(!mapActivityNew.containsKey(objActivity.Id)) {

                    sObject obj;

                    if(objActivity.get('isParent__c') == true && objActivity.get('ParentId__c') == null) {

                        obj = objType.newSObject();
                        obj = mapParentId_Activity.get(objActivity.Id).clone();

                    } else if(objActivity.get('isParent__c') == false && objActivity.get('ParentId__c') != null) {

                        obj = objType.newSObject();
                        obj = mapParentId_Activity.get((Id)objActivity.get('ParentId__c')).clone();                    
                    }

                    if(obj != null) {

                        obj.Id = objActivity.Id;
                        obj.put('isParent__c', objActivity.get('isParent__c'));
                        obj.put('ParentId__c', objActivity.get('ParentId__c'));
                        obj.put('WhatId', objActivity.get('WhatId'));
                        obj.put('WhoId', objActivity.get('WhoId'));                        

                        //Set the old status, if closeRelatedActivities is false
                        if(objType == Task.SObjectType && closeRelatedActivities == false) {

                            obj.put('Status', objActivity.get('Status'));
                        }

                        //Do not copy IsReminderSet value if Do_Not_Set_Reminder_To_Child_Activities__c value is true
                        if(SL_Meeting_Notes_Settings__c.getInstance().Do_Not_Set_Reminder_To_Child_Activities__c == true) {

                            obj.put('IsReminderSet', objActivity.get('IsReminderSet'));
                        }

                        lstActivityToUpdate.add(obj);
                    }
                }
            }
        }        

        try {
            
            if(lstActivityToUpdate.size()>0)
                update lstActivityToUpdate; 

        } catch (DMLException dmle) {
            
            Trigger.new[0].addError('Error in updating related activities: '+dmle.getDmlMessage(0));
        }
    }
    
    //Static class for recursion handler
    public static boolean runOnce(){
        if(run) {
            run=false; 
            return true;
        } 
        else {
            return run;
        }
     }
}