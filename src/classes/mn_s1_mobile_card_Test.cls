@isTest
private class mn_s1_mobile_card_Test {
    
    @isTest 
    static void unitTestMN_S1_Mobile_card() {

        SL_TestDataFactory.createTestDataForMultipleTaskEvent();
        
        Account objAcc = [Select Id from Account limit 1];
        Contact objCont = [Select Id from Contact limit 1];

        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objAcc);
        mn_s1_mobile_card objMNS1MC = new mn_s1_mobile_card(sc);

        String strTaskFields = 'Id, Subject, Status, ActivityDate, LastModifiedDate';

        String strEventFields = 'Id, Subject, StartDateTime, LastModifiedDate'; 

        mn_s1_mobile_card.getGridRecords(objCont.Id, 'Open Activities', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');
        mn_s1_mobile_card.getGridRecords(objCont.Id, 'Activity History', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');
        mn_s1_mobile_card.getGridRecords(objCont.Id, 'Open Tasks', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');
        mn_s1_mobile_card.getGridRecords(objCont.Id, 'Open Events', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');
        mn_s1_mobile_card.getGridRecords(objCont.Id, 'Closed Tasks', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');
        mn_s1_mobile_card.getGridRecords(objCont.Id, 'Closed Events', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');

        mn_s1_mobile_card.getGridRecords(objAcc.Id, 'Open Activities', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');
        mn_s1_mobile_card.getGridRecords(objAcc.Id, 'Activity History', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');
        mn_s1_mobile_card.getGridRecords(objAcc.Id, 'Open Tasks', strTaskFields, strEventFields, 0, 'ActivityDate DESC');
        mn_s1_mobile_card.getGridRecords(objAcc.Id, 'Open Events', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');
        mn_s1_mobile_card.getGridRecords(objAcc.Id, 'Closed Tasks', strTaskFields, strEventFields, 0, 'ActivityDate DESC');
        mn_s1_mobile_card.getGridRecords(objAcc.Id, 'Closed Events', strTaskFields, strEventFields, 0, 'LastModifiedDate DESC');

        SL_MN_Mobile_Settings__c setting = new SL_MN_Mobile_Settings__c(Mobile_Related_List_Limit__c = 2);
        insert setting;

        mn_s1_mobile_card.getGridRecords(objCont.Id, 'Open Activities', strTaskFields, strEventFields, 1, 'LastModifiedDate DESC');

        String strResult = mn_s1_mobile_card.getGridRecords(objAcc.Id, 'Open Activities', strTaskFields, strEventFields, 1, 'ActivityDate DESC');
        
        system.assert(strResult != NULL);
        system.assert(strResult != '');
    }
    
}