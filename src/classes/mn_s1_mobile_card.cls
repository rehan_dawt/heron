public with sharing class mn_s1_mobile_card {

    public mn_s1_mobile_card(ApexPages.StandardController stdController){}

    @remoteAction
    public static String getGridRecords(String sObjectId, String activityTitle, String taskFields, String eventFields, Integer offset, String sortOrder) 
    {
        String activityType = String.valueOf(Id.valueOf(sObjectId).getSobjectType());
        List<sObject> lstTask = new List<sObject>();
        List<sObject> lstEvent = new List<sObject>(); 
        List<Sobject> lstSortedActivities = new List<Sobject>(); 
        Set<DateTime> setDateTime = new Set<DateTime>(); 
        Map<DateTime, List<Sobject>> mapDateTimeToSObject = new Map<DateTime, List<Sobject>>();
        Map<String, Object> result = new Map<String, Object>();
        Datetime dTime = Datetime.now();

        Integer limitVal = Integer.valueOf(SL_MN_Mobile_Settings__c.getInstance().Mobile_Related_List_Limit__c);
        Boolean queryAllRows = SL_MN_Mobile_Settings__c.getInstance().Closed_Activities_All_Rows__c;

        String limitCondition, queryAllRowsCondition;        

        Boolean isTaskMoreRecords = false;
        Boolean isEventMoreRecords = false;

        //Getting Limit value from Custom Setting and if greater than 0 then add 1 to the limit and query result to check and set isTaskMoreRecords to true
        //else isTaskMoreRecords will be false, please refer the below if conditions after catch block
        if(limitVal > 0 ) {
            limitCondition = ' LIMIT ' + (limitVal + 1) + ' OFFSET '+offset;
        } else if(limitVal == null || limitVal <= 0){
            limitCondition = '';     
        }   

        String orderCondition = ' ORDER BY ' + sortOrder; 

        //If Closed_Activities_All_Rows__c = true, retrieve archived records else don't query them.
        if(queryAllRows) {
            queryAllRowsCondition = ' all rows';
        } else {
            queryAllRowsCondition = '';
        }

        try {

            if(activityType == 'Contact' || activityType == 'Lead') {

                if(activityTitle == 'Open Activities') { 
                  
                    //form query that is suitable to task
                    lstTask = Database.query('SELECT '+taskFields+' FROM Task WHERE Id IN (SELECT taskId FROM taskWhoRelation WHERE RelationId =\''+sObjectId+'\') AND isDeleted = false AND IsClosed = false '+orderCondition+limitCondition);

                    //form query that is suitable to event
                    lstEvent = Database.query('SELECT '+eventFields+' FROM Event WHERE Id IN (SELECT eventId FROM eventWhoRelation WHERE RelationId = \''+sObjectId+'\') AND isDeleted = false AND StartDateTime >: dTime ' +orderCondition+limitCondition);

                } else if(activityTitle == 'Activity History') {

                    //form query that is suitable to task
                    lstTask = Database.query('SELECT '+taskFields+' FROM Task WHERE Id IN (SELECT taskId FROM taskWhoRelation WHERE RelationId =\''+sObjectId+'\') AND isDeleted = false AND IsClosed = true '+orderCondition+limitCondition+queryAllRowsCondition);

                    //form query that is suitable to event
                    lstEvent = Database.query('SELECT '+eventFields+' FROM Event WHERE Id IN (SELECT eventId FROM eventWhoRelation WHERE RelationId = \''+sObjectId+'\') AND isDeleted = false AND StartDateTime <: dTime ' +orderCondition+limitCondition+queryAllRowsCondition);
                    
                } else if(activityTitle == 'Open Tasks') {

                    //form query that is suitable to task
                    lstTask = Database.query('SELECT '+taskFields+' FROM Task WHERE Id IN (SELECT taskId FROM taskWhoRelation WHERE RelationId =\''+sObjectId+'\') AND isDeleted = false AND IsClosed = false '+orderCondition+limitCondition);

                } else if(activityTitle == 'Open Events') {

                    //form query that is suitable to event
                    lstEvent = Database.query('SELECT '+eventFields+' FROM Event WHERE Id IN (SELECT eventId FROM eventWhoRelation WHERE RelationId = \''+sObjectId+'\') AND isDeleted = false AND StartDateTime >: dTime ' +orderCondition+limitCondition);

                } else if(activityTitle == 'Closed Tasks') {
                    
                    //form query that is suitable to task
                    lstTask = Database.query('SELECT '+taskFields+' FROM Task WHERE Id IN (SELECT taskId FROM taskWhoRelation WHERE RelationId =\''+sObjectId+'\') AND isDeleted = false AND IsClosed = true '+orderCondition+limitCondition+queryAllRowsCondition);
                    
                } else if(activityTitle == 'Closed Events') {
                    
                    //form query that is suitable to event
                    lstEvent = Database.query('SELECT '+eventFields+' FROM Event WHERE Id IN (SELECT eventId FROM eventWhoRelation WHERE RelationId = \''+sObjectId+'\') AND isDeleted = false AND StartDateTime <: dTime ' +orderCondition+limitCondition+queryAllRowsCondition);
                }

            } else { //if record type is other than contact or lead

                String queryCondition = 'WhatId=\''+sObjectId+'\'';              
                    
                if(activityType == 'Account') {

                    queryCondition = '('+ queryCondition + ' OR AccountId=\''+sObjectId+'\')';
                }

                if(activityTitle == 'Open Activities') {

                    //form query that is used to get the open tasks
                    lstTask = Database.query('SELECT '+taskFields+' FROM Task WHERE '+queryCondition+ ' AND isDeleted = false AND Status != \'Completed\' AND IsClosed = false  '+orderCondition+limitCondition);

                    //form query that is used to get the open events
                    lstEvent = Database.query('SELECT '+eventFields+' FROM Event WHERE '+queryCondition+ '  AND isDeleted = false AND StartDateTime >: dTime '+orderCondition+limitCondition); 

                } else if(activityTitle == 'Activity History') {

                    //form query that is used to get the closed tasks
                    lstTask = Database.query('SELECT '+taskFields+' FROM Task WHERE '+queryCondition+ ' AND isDeleted = false AND Status = \'Completed\' AND IsClosed = true  '+orderCondition+limitCondition+queryAllRowsCondition);

                    //form query that is used to get the closed events
                    lstEvent = Database.query('SELECT '+eventFields+' FROM Event WHERE '+queryCondition+ '  AND isDeleted = false AND StartDateTime <: dTime  '+orderCondition+limitCondition+queryAllRowsCondition);

                } else if(activityTitle == 'Open Tasks') {

                    //form query that is used to get the open tasks
                    lstTask = Database.query('SELECT '+taskFields+' FROM Task WHERE '+queryCondition+ ' AND isDeleted = false AND Status != \'Completed\' AND IsClosed = false  '+orderCondition+limitCondition);

                } else if(activityTitle == 'Open Events') {

                    //form query that is used to get the open events
                    lstEvent = Database.query('SELECT '+eventFields+' FROM Event WHERE '+queryCondition+ '  AND isDeleted = false AND StartDateTime >: dTime '+orderCondition+limitCondition); 

                } else if(activityTitle == 'Closed Tasks') {
                    
                    //form query that is used to get the closed tasks
                    lstTask = Database.query('SELECT '+taskFields+' FROM Task WHERE '+queryCondition+ ' AND isDeleted = false AND Status = \'Completed\' AND IsClosed = true  '+orderCondition+limitCondition+queryAllRowsCondition);

                } else if(activityTitle == 'Closed Events') {
                    
                    //form query that is used to get the closed events
                    lstEvent = Database.query('SELECT '+eventFields+' FROM Event WHERE '+queryCondition+ '  AND isDeleted = false AND StartDateTime <: dTime  '+orderCondition+limitCondition+queryAllRowsCondition);
                }
            }
        } catch (QueryException qe) {
            return '[{"message":"' + qe.getMessage() + '","errorCode":"INVALID_QUERY"}]';
        }

        //To check the list size greater than limit value then set isTaskMoreRecords to true
        //else isTaskMoreRecords will be false
        if(limitVal != null && limitVal > 0) {
            if(lstTask.size() > limitVal) {
                isTaskMoreRecords = true;
            }
            if(lstEvent.size() > limitVal) {
                isEventMoreRecords = true;
            }  
        }       
        

        // This is to prepare custom sort functionality in case of Open Activity/Activity History(Task+Event records)
        if(activityTitle == 'Open Activities' || activityTitle == 'Activity History'){
            
            // preparing iterationNo to iterate over lstTask, checking if limitVal <= list size then using same limit val as limitVal should not greater than list size
            // else using list size 
            Integer iterationNo = (limitVal != null && limitVal <= lstTask.size()) ? limitVal : lstTask.size();
            DateTime dateTimeValue;
            
            for(Integer i = 0; i < iterationNo; i++){

                //checking the task sort field type is DATE then convert them into DateTime as direct assignment to DateTime is not possible in case of get()
                //else if DATETIME then assigning directly and then adding them into Datetime set and map to sort them
                if(String.valueOf(Schema.SObjectType.Task.fields.getMap().get(sortOrder.split(' ')[0]).getDescribe().getType()) == 'DATE') {
                    Date dateValue = Date.valueOf(lstTask[i].get(sortOrder.split(' ')[0]));
                        
                    if(dateValue != null) {

                        dateTimeValue = DateTime.newInstance(dateValue.year(),dateValue.month(),dateValue.day()); 
                    }

                } else if(String.valueOf(Schema.SObjectType.Task.fields.getMap().get(sortOrder.split(' ')[0]).getDescribe().getType()) == 'DATETIME') {

                    dateTimeValue = DateTime.valueOf(lstTask[i].get(sortOrder.split(' ')[0]));
                }

                setDateTime.add(dateTimeValue);            
                
                if(!mapDateTimeToSObject.containsKey(DateTime.valueOf(dateTimeValue)))              
                    mapDateTimeToSObject.put(DateTime.valueOf(dateTimeValue), new List<Sobject>());   
               
                mapDateTimeToSObject.get(DateTime.valueOf(dateTimeValue)).add(lstTask[i]);
                
            }
            
            iterationNo = (limitVal != null && limitVal <= lstEvent.size()) ? limitVal : lstEvent.size();
            
            for(Integer i = 0; i < iterationNo; i++){

                //checking the event sort field type is DATE then convert them into DateTime as direct assignment to DateTime is not possible in case of get()
                //else if DATETIME then assigning directly and then adding them into Datetime set and map to sort them
                if(String.valueOf(Schema.SObjectType.Event.fields.getMap().get(sortOrder.split(' ')[0]).getDescribe().getType()) == 'DATE') {
                    Date dateValue = Date.valueOf(lstEvent[i].get(sortOrder.split(' ')[0]));
                        
                    if(dateValue != null) {

                        dateTimeValue = DateTime.newInstance(dateValue.year(),dateValue.month(),dateValue.day()); 
                    }

                } else if(String.valueOf(Schema.SObjectType.Event.fields.getMap().get(sortOrder.split(' ')[0]).getDescribe().getType()) == 'DATETIME') {

                    dateTimeValue = DateTime.valueOf(lstEvent[i].get(sortOrder.split(' ')[0]));
                }
                setDateTime.add(dateTimeValue);            
                
                if(!mapDateTimeToSObject.containsKey(DateTime.valueOf(dateTimeValue)))              
                    mapDateTimeToSObject.put(DateTime.valueOf(dateTimeValue), new List<Sobject>());   
               
                mapDateTimeToSObject.get(DateTime.valueOf(dateTimeValue)).add(lstEvent[i]);
                
            }   

            List<DateTime> lstDateTimeToSort = new List<DateTime>(setDateTime);
            lstDateTimeToSort.sort();

            for(DateTime dt : lstDateTimeToSort){
               
                lstSortedActivities.addAll(mapDateTimeToSObject.get(dt));
            }

            result.put('records', lstSortedActivities);
            
        }
        // This is to directly pass the sorted query result when only Task or Event (Open Task, Closed Task, Open Event, Closed Event)
        else{
            
            if(!lstTask.isEmpty()){
                
                // If limit is less than list size then remove the extra query which we added above to get isTaskMoreRecords
                if(limitVal != null && limitVal < lstTask.size())
                    lstTask.remove(lstTask.size() - 1);
                    
                lstSortedActivities.addAll(lstTask);
            }   
            else if(!lstEvent.isEmpty()){
                
                // If limit is less than list size then remove the extra query which we added above to get isEventMoreRecords
                if(limitVal != null && limitVal < lstEvent.size())
                    lstEvent.remove(lstEvent.size() - 1);
                
                lstSortedActivities.addAll(lstEvent);   
            }

            result.put('records', lstSortedActivities);
        }

        result.put('isTaskMoreRecords', isTaskMoreRecords);
        result.put('isEventMoreRecords', isEventMoreRecords);

        return JSON.Serialize(result);
    }   
}