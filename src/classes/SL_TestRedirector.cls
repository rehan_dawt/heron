@isTest
public class SL_TestRedirector {

    @isTest 
    static void unitTestRedirector() {
    
    SL_TestDataFactory.createTestDataForEvent();
    
    Task objTask = [Select Id from Task limit 1];
    
    PageReference pageRef = Page.SL_MeetingNoteTask;
    pageRef.getParameters().put('id', objTask.Id);

    Event objEvent = [Select Id from Event limit 1];
    
    Test.setCurrentPage(pageRef);

    SL_Meeting_Notes_Settings__c cs = [Select Id from SL_Meeting_Notes_Settings__c limit 1];
    
    Test.startTest();

    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new Task());
    SL_Redirector redir = new SL_Redirector(sc);   
    redir.redirect();
    
    system.assertEquals(redir.pgRef.getParameters().get('id') , objTask.Id, 'Éxpecting Task Id should be same as Page Parameter');

    sc = new ApexPages.Standardcontroller(new Event());
    
    pageRef.getParameters().remove('id');

    redir = new SL_Redirector(sc);
    redir.redirect();
    
    system.assertEquals(redir.pgRef.getParameters().get('nooverride'), '1', '');

    pageRef = Page.SL_MeetingNoteEvent;
    redir = new SL_Redirector(sc);
    redir.hasEditAccess(objEvent.Id);
    redir.redirect();
    
    system.assertEquals(redir.pgRef.getParameters().get('nooverride'), '1', '');

    Test.stopTest();

    }
}