global without sharing class SL_AutoCompleteNoQuery_Controller
{	
	@TestVisible 
	@RemoteAction
	global static String[] getStrings(String[] queryMatches, String strSearch) {
		List<String> matchStrings = new List<String>();
		for (String s: queryMatches){
			if (s.toLowerCase().startsWith(strSearch.toLowerCase())){
				matchStrings.add(s);
			}
		}
		return matchStrings;
	}
	/* End - Method */

}