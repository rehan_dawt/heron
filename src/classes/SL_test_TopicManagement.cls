@isTest
public class SL_test_TopicManagement {
    @testSetup 
    static void setup() {
        SL_TestDataFactory.createTestDataForTopicManagement();
    }

    @isTest 
    static void testPageLoad(){
        List<SL_ctrl_TopicManagement.TopicItem> unapprovedTopics = SL_ctrl_TopicManagement.getUnapprovedTopics();
        List<SL_ctrl_TopicManagement.TopicItem> approvedTopics = SL_ctrl_TopicManagement.getApprovedTopics();
        SL_ctrl_TopicManagement.getCategories();
        
        System.assertEquals(unapprovedTopics.size(), 2);
        System.assertEquals(approvedTopics.size(), 2);
    }
    
    @isTest 
    static void testEditTopic(){         
        Account a = [Select Id From Account Limit 1];
        FeedItem fi = new FeedItem(Body = '#[Test Topic 1] #[Test Topic 2] bleh I hate tests', ParentId = a.Id);
        insert fi;
        
        List<SL_ctrl_TopicManagement.TopicItem> unapprovedTopics = SL_ctrl_TopicManagement.getUnapprovedTopics();
        List<Id> unapp = new List<Id>();
        for(SL_ctrl_TopicManagement.TopicItem ti : unapprovedTopics){
            unapp.add(ti.DT_RowId);
        }
        SL_ctrl_TopicManagement.editTopic(unapp, 'unapp', '');
        
        List<SL_ctrl_TopicManagement.TopicItem> approvedTopics = SL_ctrl_TopicManagement.getApprovedTopics();
        List<Id> app = new List<Id>();
        for(SL_ctrl_TopicManagement.TopicItem ti : approvedTopics){
            app.add(ti.DT_RowId);
        }
        
        Map<String, List<SL_ctrl_TopicManagement.TopicItem>> mapEditTask = SL_ctrl_TopicManagement.editTopic(app, 'app', 'Featured');
        System.assertEquals(mapEditTask.size(), 2, 'Expecting Edit Task Records');
    }
    
    @isTest 
    static void testApproveTopic(){
        List<String> unapproved = new List<String>();
        for(SL_ctrl_TopicManagement.TopicItem ti : SL_ctrl_TopicManagement.getUnapprovedTopics()){
            unapproved.add(ti.Name);
        }
        
        Map<String, List<SL_ctrl_TopicManagement.TopicItem>> mapApproveTask = SL_ctrl_TopicManagement.approveTopic(unapproved, 'Featured');
        System.assertEquals(mapApproveTask.size(), 2, 'Expecting Approved Task Records');
    }
    
    @isTest 
    static void testUnapproveTopic(){
        List<String> approved = new List<String>();
        for(SL_ctrl_TopicManagement.TopicItem ti : SL_ctrl_TopicManagement.getApprovedTopics()){
            approved.add(ti.Name);
        }
        
        List<String> test = new List<String>();
        test.add('Featured');
        
        Map<String, List<SL_ctrl_TopicManagement.TopicItem>> mapUnApproveTask = SL_ctrl_TopicManagement.unapproveTopic(approved, test);
        System.assertEquals(mapUnApproveTask.size(), 2, 'Expecting UnApproved Task Records');
    }
}