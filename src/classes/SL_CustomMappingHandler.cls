public with sharing class SL_CustomMappingHandler {

    private Boolean isExecuting;
    private Integer batchSize;
    public SL_CustomMappingHandler(Boolean isExecuting, Integer batchSize) {
        this.isExecuting = isExecuting;
        this.batchSize = batchSize;
    }

    public void onBeforeInsert(List<Custom_Mapping__c> customMappingsToInsert){
        SL_MappingValidationUtils.customMappingValidate(customMappingsToInsert);
    }

    public void onBeforeUpdate(Map<Id, Custom_Mapping__c> oldCustomMappings, Map<Id, Custom_Mapping__c> newCustomMappings){
        SL_MappingValidationUtils.customMappingValidate(newCustomMappings.values());
    }
}