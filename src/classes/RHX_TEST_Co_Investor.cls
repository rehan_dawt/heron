@isTest(SeeAllData=true)
public class RHX_TEST_Co_Investor 
{
    @isTest 
    static void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
            FROM Co_Investor__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Co_Investor__c()
            );
        }
        
        system.assertEquals(sourceList.size(), 1);
        
        rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}