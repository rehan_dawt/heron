// contains a record that has been generated from a conversion and its relevant mapping data
global class SL_MappedRecordWrapper {

    private final SObject newSObject;
    private final SObject originatingSObject;
    private final String sObjectAPIName;
    private final String lookupFieldToParentRecord; // parent wrapper calls 
    private final List<SL_MappedRecordWrapper> children;
    private Boolean toInsert;

    global SL_MappedRecordWrapper(SObject newSObject, SObject originatingSObject, String sObjectAPIName, String lookupFieldToParentRecord){
        this.newSObject = newSObject;
        this.originatingSObject = originatingSObject;
        this.sObjectAPIName = sObjectAPIName;
        this.lookupFieldToParentRecord = lookupFieldToParentRecord;
        children = new List<SL_MappedRecordWrapper>();
        toInsert = true;
    }
    
    global SL_MappedRecordWrapper(SObject newSObject, String sObjectAPIName){
         this.newSObject = newSObject;
         this.sObjectAPIName = sObjectAPIName;
         toInsert = true;
    }
    
    global SObject getSObject(){
        return newSObject;
    }

    global SObject getOriginatingSObject(){
        return originatingSObject;
    }

    global String getsObjectAPIName(){
        return sObjectAPIName;
    }
    global String getLookupFieldToParentRecord(){
        return lookupFieldToParentRecord;
    }

    global List<SL_MappedRecordWrapper> getChildren(){
        if (newSObject.get('id')!=null){
            for (SL_MappedRecordWrapper child: children){
                String lookupField = child.getLookupFieldToParentRecord();
                if (lookupField!=null && child.getSObject().get(lookupField)==null){
                    child.getSObject().put(child.getLookupFieldToParentRecord(), newSObject.Id);
                }
            }
        }
        return children;
    }

    global void addChild(SL_MappedRecordWrapper child){
        children.add(child);
    }

    global Boolean toInsert(){
        return toInsert;
    }

    global void doNotInsert(){
        toInsert=false;
    }

}