@isTest(SeeAllData=true)
public class RHX_TEST_CampaignInfluence 
{
    @isTest 
    static void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
            FROM CampaignInfluence ci, ci.Opportunity o
            WHERE o.Converted__c = false AND o.StageName = 'Prospect'
            LIMIT 1];
        if(sourceList.size() == 0) {
            Opportunity objOpp = new Opportunity(Name='Test Opp', StageName = 'Prospect', CloseDate=date.today());
            insert objOpp;
            
            Campaign objCamp = new Campaign(Name='Test Campaign');
            insert objCamp;
            
            CampaignInfluenceModel objCampaignInfluenceModel = [Select Id from CampaignInfluenceModel limit 1];
            
            sourceList.add(
                    new CampaignInfluence(OpportunityId=objOpp.Id, CampaignId=objCamp.Id, ModelId = objCampaignInfluenceModel.Id)
            );
        }
        
        system.assertEquals(sourceList.size(), 1);
        
        rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}