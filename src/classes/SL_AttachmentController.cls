/*
 * Controller for attachment component
 */
 
public with sharing class SL_AttachmentController 
{
    // the parent object id where Attachment will be attached. This will be either passed from iframe param or JS method fom Save method.
    public String sobjId {get; set;}

    // String of removed attachments id's. This will be passed from JS method.
    public String strRemovedAttachments {get; set;}
    
    // list of existing attachments - populated on demand
   // public Attachment attachment {get; set;}
    public List<Attachment> lstAttachments {get;set;}
    
    // list of new attachments to add
    public List<Attachment> attachments;

    // constructor
    public SL_AttachmentController()  
    {
        sobjId = '';  

        attachments = new List<Attachment>();
        
        if(ApexPages.currentPage().getParameters().containsKey('id') && ApexPages.currentPage().getParameters().get('id') != '')
            sobjId = Id.valueOf(ApexPages.currentPage().getParameters().get('id'));
        
        // instantiate the list with a single attachment
        //attachment = new Attachment();
        lstAttachments = new List<Attachment>();
    }   
    
    // retrieve the existing attachments
    public List<Attachment> getAttachments() 
    {
        
        Integer maxNoOfAttachment = 1;
        Integer valueFromCustomSetting = Integer.valueOf(SL_Meeting_Notes_Settings__c.getInstance().Maximum_Number_Of_Attachments_In_One_Go__c);
        
        //To force the maximum values as always 10
        if(valueFromCustomSetting != null){
        
            maxNoOfAttachment = valueFromCustomSetting > 10 ? 10 : valueFromCustomSetting;
        }
        
        for (Integer idx = 0; idx < maxNoOfAttachment; idx++) {
            
            lstAttachments.add(new Attachment());
        }

        // only execute the SOQL if the list hasn't been initialised
        attachments = [SELECT Id, ParentId, Name, Description, CreatedDate, LastModifiedDate, CreatedBy.Name, LastModifiedBy.Name FROM Attachment WHERE parentId = :sobjId];

        return attachments;
    }

    //refresh attachments table
    public void refreshAttachments()
    {
        lstAttachments = new List<Attachment>(); 
    }

    // Save action method
    public Pagereference save()
    {
        try{

            if(String.isNotBlank(sobjId)){
                
                if(String.isNotBlank(strRemovedAttachments)) {

                    List<Attachment> lstAttachmentsToBeDeleted = new List<Attachment>();
                    for(String strAttachmentId :strRemovedAttachments.split(',')){

                        lstAttachmentsToBeDeleted.add(new Attachment(Id = strAttachmentId));
                    }
                    
                    delete lstAttachmentsToBeDeleted;
                }    

                System.debug('== lstAttachments ==='+lstAttachments); 

                List<Attachment> toInsert=new List<Attachment>();
                Id ownerId; 

                if(sobjId.substring(0, 3) == '00T')
                    ownerId = [SELECT OwnerId FROM Task WHERE Id =: sobjId ALL ROWS].OwnerId;
                else if(sobjId.substring(0, 3) == '00U')
                    ownerId = [SELECT OwnerId FROM Event WHERE Id =: sobjId ALL ROWS].OwnerId;

                for (Attachment newAtt : lstAttachments)
                {
                    if (newAtt.Body!=null)
                    {
                        newAtt.parentId = sobjId;
                        newAtt.OwnerId = ownerId;
                        toInsert.add(newAtt);
                    }
                }
                insert toInsert;
                lstAttachments = new List<Attachment>();
                
                // null the list of existing attachments - this will be rebuilt when the page is refreshed
                attachments=null;

                return new Pagereference('/apex/AttachmentRelatedList?isSuccess=true&msg=attached sucessfully');
            }
            else{
                //attachment = new Attachment();
                return new Pagereference('/apex/AttachmentRelatedList?isSuccess=false&msg=Parent doesnt found');
            }  
        } 
        catch(DMLException e){
            
            lstAttachments = new List<Attachment>();
            return new Pagereference('/apex/AttachmentRelatedList?isSuccess=false&msg=DML exception while attaching');
        
        }            
    }

}