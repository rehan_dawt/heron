public with sharing class SL_Redirector {

  public PageReference pgRef;

  public SL_Redirector(ApexPages.StandardController stdController) {  
     
    SL_Meeting_Notes_Settings__c cs = SL_Meeting_Notes_Settings__c.getInstance();

    if(cs != null && cs.SL_Redirector__c) {

      Map<String, String> pageParams = ApexPages.currentPage().getParameters();

      String id = pageParams.get('id');

      if(id != null) {

        if(ApexPages.currentPage().getURL().contains('retURL') && hasEditAccess(id) ) {

          pgRef = new PageReference('/'+id+'/e'); 

        } else {

          pgRef = new PageReference('/'+id); 
        }
        
      } else {

        if(ApexPages.currentPage().getURL().contains('MeetingNoteEvent')) {

          pgRef = new PageReference('/00U/e');

        } else {

          pgRef = new PageReference('/00T/e');
        }
      }

      pgRef.getParameters().putAll(pageParams);
      pgRef.getParameters().put('nooverride', '1');     
      pgRef.getParameters().remove('save_new');
    } 
  }

  @TestVisible
  private Boolean hasEditAccess(Id recordId) {
    // User doesn't have edit access on child group events  
    if(recordId.getSObjectType() == Schema.Event.SObjectType) { 
      Event evt = [SELECT isGroupEvent, ownerId, IsChild FROM Event WHERE Id = :recordId];
            
      if(evt.isGroupEvent && evt.IsChild && evt.ownerId == UserInfo.getUserId() ) {
        return false;
      }
    }
    
    return true;
  }

  public PageReference redirect() {
    
    if(pgRef != null) pgRef.setRedirect(true);
    return pgRef;
  
  }
}