public with sharing class SL_CA_Mapping {
    public Custom_Mapping__c customMapping                                  {get;set;}
    public List<SelectOption> sObjectOptions                                {get;set;}
    public List<SelectOption> childOptions                                  {get;set;}
    public String selectedObject                                            {get;set;}
    public String sourceObject                                              {get;set;}
    public String newTargetObject                                           {get;set;}
    public String strNameSpacePrefix                                        {get;set;}
    private Map<SL_ObjectRelationshipWrapper, Boolean> allCheckboxSelect;
    private Map<String, SL_ObjectRelationshipWrapper> directTargetObjects; // these are the target object api names in all direct association object relationships
    private List<SelectOption> directTargetOptions;
    private List<SelectOption> directTargetOptionsRelatedList;

    public List<SL_ObjectRelationshipWrapper> objectRelationships           {   get
                                                                                {objectRelationships.sort(); return objectRelationships;} 
                                                                                set;
                                                                            }

    public SL_CA_Mapping(){
        this(new Custom_Mapping__c (Name = '', SObject_Type__c = ''));     
    }
    
    public SL_CA_Mapping(ApexPages.StandardController stdController) {
        this((Custom_Mapping__c) stdController.getRecord());
    }
    
    public SL_CA_Mapping(Custom_Mapping__c customMapping){
        directTargetObjects = new Map<String, SL_ObjectRelationshipWrapper>();
        directTargetOptions = new List<SelectOption>{new SelectOption('', '--None--')};
        directTargetOptionsRelatedList = new List<SelectOption>();
        strNameSpacePrefix = '';
        List<ApexPage> lstApexPage = [SELECT Id, NamespacePrefix FROM ApexPage WHERE Name = 'SL_CA_Mapping' LIMIT 1];
        strNameSpacePrefix = (!lstApexPage.isEmpty() && lstApexPage[0].NamespacePrefix != null) ? (lstApexPage[0].NamespacePrefix + '__') : '';
        
        Map<String, Schema.SObjectType> allSObjects = Schema.getGlobalDescribe();
        Id customMappingId = ApexPages.currentPage().getParameters().get('id');
        
        if(ApexPages.currentPage().getParameters().get('cancel')=='true'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Changes discarded.'));
        }
        
        // when there's an ID url parameter
        if (customMappingId != null){
            List<Custom_Mapping__c> temp = [SELECT Id, Name, Custom_Mapping_Name__c, Active__c, SObject_Type__c FROM Custom_Mapping__c WHERE Id =: customMappingId LIMIT 1];
            if (!temp.isEmpty()){
                customMapping = temp[0];
            }
        }
        this.customMapping = customMapping;
        // defaults the picklist to the correct value
        selectedObject = this.customMapping.SObject_Type__c!=null ? this.customMapping.SObject_Type__c.toLowerCase() : '';
       
        // called to query all relevant object relationships and field mappings
        buildCustomMappingHierarchy(this.customMapping, allSObjects);
        
        // generate the list of all sobjects to be selected from
        sObjectOptions = new List<SelectOption>();
        sObjectOptions.add(new SelectOption('', '--None--'));
        Map<String, List<String>> labelToAPI = new Map<String, List<String>>();
        
        for (String sObjName: allSObjects.keySet()){
            String label = allSObjects.get(sObjName).getDescribe().getLabel();
            if (!labelToAPI.containsKey(label)){
                labelToAPI.put(label, new List<String>{sObjName});
            }
            else {
                labelToAPI.get(label).add(sObjName);
            }   
        }
        
        List<String> sortedObjectLabels = new List<String>(labelToAPI.keySet());
        sortedObjectLabels.sort();
        for(String sObjLabel : sortedObjectLabels){
            for (String api: labelToAPI.get(sObjLabel))
                sObjectOptions.add(new SelectOption(api,sObjLabel));
        }
    }

    public void buildCustomMappingHierarchy(Custom_Mapping__c customMapping, Map<String, Schema.SObjectType> allSObjects){
        objectRelationships = new List<SL_ObjectRelationshipWrapper>();
        allCheckboxSelect = new Map<SL_ObjectRelationshipWrapper, Boolean>();
        Id customMappingId = customMapping.Id;
        for (Object_Relationship__c objectRelationship:  
                            [   SELECT  Name,
                                        Custom_Mapping__c,
                                        Context_Object_API__c,
                                        Context_Relationship_Id__c,
                                        Parent_Object_Relationship__c,
                                        Parent_Object_Relationship__r.RecordType.DeveloperName,
                                        Parent_Object_Relationship__r.Target_Object_API__c,
                                        Related_Record_Parent_Id__c,
                                        Target_Object_API__c,
                                        Target_Relationship_Id__c,
                                        Target_Parent_Lookup__c,
                                        Copy_Attachments__c,
                                        Copy_Notes__c,
                                        Count_Field_Mappings__c,
                                        Order__c,
                                        RecordType.DeveloperName,
                                        RecordTypeId,
                                        Active__c,
                                        Map_Record_Type__c,
                                        (
                                        SELECT  Active__c,
                                                Context_Field_Name__c, 
                                                Target_Field_Name__c, 
                                                Target_Value__c,
                                                RecordTypeId
                                        FROM    Field_Mappings__r
                                        )
                                FROM    Object_Relationship__c
                                WHERE   Custom_Mapping__c=: customMappingId
                            ]){
            SL_ObjectRelationshipWrapper newWrapper;
            if (objectRelationship.RecordType.DeveloperName=='Direct_Association'){
                String targetAPI = objectRelationship.Target_Object_API__c;
                newWrapper = new SL_ObjectRelationshipWrapper(objectRelationship, directTargetOptions, false);
                if (!directTargetObjects.containsKey(targetAPI)){
                    directTargetObjects.put(targetAPI, newWrapper);
                    SelectOption newOption = new SelectOption(targetAPI, allSObjects.get(targetAPI).getDescribe().getLabel());
                    directTargetOptions.add(newOption);
                    directTargetOptionsRelatedList.add(newOption);
                }
            }
            else {
                newWrapper = new SL_ObjectRelationshipWrapper(objectRelationship, directTargetOptionsRelatedList, true);
            }
            if (newWrapper.errorMessage!=null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Some mappings below are not functional.  Dysfunctional mappings have been inactivated.  Please resolve.'));
            }
            objectRelationships.add(newWrapper);
            allCheckboxSelect.put(newWrapper, false);
                
        }
        
        // if we're not dealing with the dummy custom mapping
        if (customMapping.SObject_Type__c!=null && customMapping.SObject_Type__c!=''){
            childOptions = new List<SelectOption>();
            DescribeSObjectResult objectDescribe = allSObjects.get(customMapping.SObject_Type__c).getDescribe();
            
            // add the object itself as a possibility for a new object relationship source object
            childOptions.add(new SelectOption(customMapping.SObject_Type__c,objectDescribe.getLabel()));
            List<Schema.ChildRelationship> childrenObjects = objectDescribe.getChildRelationships();
            Map <String, String> labelToAPI = new Map<String, String>();
            
            // get all the child options, sort them.
            for (Schema.ChildRelationship childObject: childrenObjects){
                // can also grab the field options while we're at it, maybe map them
                DescribeSObjectResult childObjectDecribe = childObject.getChildSObject().getDescribe();
                labelToAPI.put(childObjectDecribe.getLabel(), childObjectDecribe.getName());
            }
            List<String> sortedChildLabels = new List<String> (labelToAPI.keySet());
            sortedChildLabels.sort();
            
            for (String label: sortedChildLabels){
                // add all the child objects as potential new object relationship source objects
                childOptions.add(new SelectOption(labelToAPI.get(label), label));
            }   
        }    
    }
    // called when user hits 'select' button for now after selecting an object from the list
    public PageReference chooseSObject(){
        customMapping = new Custom_Mapping__c(SObject_Type__c=selectedObject, Active__c = true);
        buildCustomMappingHierarchy(customMapping, Schema.getGlobalDescribe());    
        return null;
    }
    
    public List<SelectOption> getRefererenceValueOptions(){
        return new List<SelectOption>{new SelectOption(SL_ObjectRelationshipWrapper.referenceRecordTypeId, 'Reference'), new SelectOption(SL_ObjectRelationshipWrapper.valueRecordTypeId, 'Value')};
    }
    
    // when a user hits 'select' on the add new object relationship section
    public void chooseTarget(){
        if (newTargetObject==''||newTargetObject==null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You must select a target!')); 
        }
        else{
            List<RecordType> objectRelRecordTypes = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Object_Relationship__c' OR SObjectType='SL_Convert__Object_Relationship__c'];
            Id newRecordTypeId;
            SL_ObjectRelationshipWrapper newWrapper;
            
            if(sourceObject==customMapping.SObject_Type__c){
                newRecordTypeId = objectRelRecordTypes[0].DeveloperName == 'Direct_Association' ? objectRelRecordTypes[0].Id : objectRelRecordTypes[1].Id;

                newWrapper = new SL_ObjectRelationshipWrapper(
                                        new Object_Relationship__c(
                                            Context_Object_API__c = customMapping.SObject_Type__c,
                                            Target_Object_API__c = newTargetObject,
                                            Context_Relationship_Id__c = 'Id',
                                            RecordTypeId = newRecordTypeId,
                                            Active__c = true),
                                        directTargetOptions,
                                        false // direct association
                );
                if (!directTargetObjects.containsKey(newTargetObject)){
                    directTargetObjects.put(newTargetObject, newWrapper);
                    SelectOption newOption = new SelectOption(newTargetObject, Schema.getGlobalDescribe().get(newTargetObject).getDescribe().getLabel());
                    directTargetOptions.add(newOption);
                    directTargetOptionsRelatedList.add(newOption);
                }
            }
            else {
                newRecordTypeId = objectRelRecordTypes[0].DeveloperName == 'Related_List' ? objectRelRecordTypes[0].Id : objectRelRecordTypes[1].Id;
                newWrapper = new SL_ObjectRelationshipWrapper(
                                        new Object_Relationship__c( 
                                            Context_Object_API__c = customMapping.SObject_Type__c,
                                            Target_Object_API__c = newTargetObject,
                                            Context_Relationship_Id__c = sourceObject,
                                            RecordTypeId = newRecordTypeId,
                                            Active__c = true),
                                        directTargetOptionsRelatedList,
                                        true // related
                );
            }
            objectRelationships.add(newWrapper);
            allCheckboxSelect.put(newWrapper, false);
        }
        newTargetObject='';
    }

    private Boolean isSaveError (Map<String, Schema.SObjectType> allSObjects){
        SObjectType customMapType = allSObjects.get(customMapping.SObject_Type__c);
        Boolean error = false;
        if (customMapType==null){
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Must select a Source Object before saving!')); 
        }
        else if (customMapping.Custom_Mapping_Name__c == null || customMapping.Name == null){
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Must enter a custom mapping name before saving!')); 
        }

        for (SL_ObjectRelationshipWrapper objectRelationshipWrapper: objectRelationships){
            if (objectRelationshipWrapper.toDelete) continue;
            Object_Relationship__c objectRelationship = objectRelationshipWrapper.getRecord();
            if (objectRelationship.RecordType.DeveloperName == 'Direct_Association' && objectRelationship.Target_Relationship_Id__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields on all relationships!')); 
                error = true;
                break;
            }
            if (objectRelationship.RecordType.DeveloperName == 'Related_List' && (objectRelationship.Target_Parent_Lookup__c == null || objectRelationship.Related_Record_Parent_Id__c == null)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill out all required fields on all relationships!')); 
                error = true;
                break;
            }
        }
        return error;
    }

    // when a user hits save, updates, inserts, and deletes all appropriate custom mapping, object relationship and field mapping records
    public void performSave(){
        SavePoint savePoint = Database.setSavePoint();
        try{
            Map<String, Schema.SObjectType> allSObjects = Schema.getGlobalDescribe();
            
            if (isSaveError(allSObjects)){
                return;
            }

            upsert customMapping;

            List<Object_Relationship__c> objectRelsToUpsert = new List<Object_Relationship__c>();
            List<Object_Relationship__c> objectRelsToDelete = new List<Object_Relationship__c>();

            for (SL_ObjectRelationshipWrapper objectRelationshipWrapper: objectRelationships){
                Object_Relationship__c objectRelationship = objectRelationshipWrapper.getRecord();
                if (!objectRelationshipWrapper.toDelete){
                    if (objectRelationship.Id == null){
                        objectRelationship.Custom_Mapping__c = customMapping.Id;
                    }
                    objectRelsToUpsert.add(objectRelationship);
                }
                else {
                    if (!objectRelationship.isDeleted && objectRelationship.Id != null){
                        objectRelsToDelete.add(objectRelationship);
                    }
                }
            }

            upsert objectRelsToUpsert;
            delete objectRelsToDelete;

            Integer directAssociation = 0;
            Integer directAssociationChild = objectRelsToUpsert.size();
            Integer relatedList = objectRelsToUpsert.size()*2;
            Integer relatedRelatedList = objectRelsToUpsert.size()*3;

            objectRelsToUpsert.clear();

            for (SL_ObjectRelationshipWrapper objectRelationshipWrapper: objectRelationships){
                if (objectRelationshipWrapper.toDelete) continue;
                Object_Relationship__c objectRelationship = objectRelationshipWrapper.getRecord();
                SL_ObjectRelationshipWrapper parentWrapper = directTargetObjects.get(objectRelationshipWrapper.targetParentAPIName);
                objectRelationship.Parent_Object_Relationship__c = parentWrapper == null ? null : parentWrapper.objectRelationship.Id;
                // populate the order of the Object Relationship
                if (objectRelationship.RecordType.DeveloperName == 'Direct_Association' && objectRelationship.Parent_Object_Relationship__r == null){
                    if (objectRelationship.Parent_Object_Relationship__r == null){
                        objectRelationship.Order__c = directAssociation++;
                    }
                    else {
                        objectRelationship.Order__c = directAssociationChild++;
                    }
                }
                else if (objectRelationship.Parent_Object_Relationship__r != null && objectRelationship.Parent_Object_Relationship__r.RecordType.DeveloperName=='Direct_Association'){
                    objectRelationship.Order__c = relatedList++;
                }
                else{
                    objectRelationship.Order__c = relatedRelatedList++;
                }
                objectRelsToUpsert.add(objectRelationship);
            }

            upsert objectRelsToUpsert;

            Integer numFieldMapsInserted = 0;
            Integer numFieldMapsUpdated  = 0;
            Integer numFieldMapsDeleted  = 0;

            Map<Id, Field_Mapping__c> fieldMapsToCheck = new Map<Id, Field_Mapping__c>();
            List<Field_Mapping__c> fieldMapsToUpsert = new List<Field_Mapping__c>();
            List<Field_Mapping__c> fieldMapsToDelete = new List<Field_Mapping__c>();

            for (SL_ObjectRelationshipWrapper objectRelationship: objectRelationships){
                if (objectRelationship.toDelete) continue;
                List<Field_Mapping__c> fieldMappings = objectRelationship.getFieldMappingsToUpsert();
                if (!fieldMappings.isEmpty()){
                    for (Field_Mapping__c fieldMapping: fieldMappings){
                        if (fieldMapping.Id!=null){
                            fieldMapsToCheck.put(fieldMapping.Id, fieldMapping);
                        }
                        else{
                            fieldMapsToUpsert.add(fieldMapping);
                            numFieldMapsInserted++;
                        }
                    }
                    
                }
                fieldMappings = objectRelationship.getFieldMappingsToDelete();
                if (!fieldMappings.isEmpty()){
                    fieldMapsToDelete.addAll(fieldMappings);
                    numFieldMapsDeleted+=fieldMappings.size();
                }
            }
            Map<Id, Field_Mapping__c> oldFieldMaps = new Map<Id, Field_Mapping__c> (
                                    [SELECT Context_Field_Name__c,
                                            Target_Field_Name__c,
                                            Target_Value__c,
                                            Active__c
                                    FROM    Field_Mapping__c
                                    WHERE   Id IN :fieldMapsToCheck.keySet()]);
            List<Field_Mapping__c> fieldMapsTemp = new List<Field_Mapping__c>(needsUpdating(oldFieldMaps, fieldMapsToCheck));
            if (!fieldMapsTemp.isEmpty()){
                fieldMapsToUpsert.addAll(fieldMapsTemp);
            }
            numFieldMapsUpdated += fieldMapsTemp.size();
            // insert/update/delete field mappings
            delete fieldMapsToDelete;
            upsert fieldMapsToUpsert;

            String message = 'Custom Mapping for '+getObjectName()+' updated: Inserted '
            + numFieldMapsInserted + ' field mapping(s), Updated '+numFieldMapsUpdated
            +' field mapping(s), Deleted '+numFieldMapsDeleted+' field mapping(s).';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, message)); 
            buildCustomMappingHierarchy(customMapping, allSObjects);
        }
        catch(Exception ex){
            System.debug(LoggingLevel.error, ex.getMessage());
            String prettyMessage = prettify(ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, prettyMessage)); 
            Database.rollback(savePoint);
        }
    }

    public String prettify(String unpretty){
        String pretty = unpretty;
        if (pretty.contains('DUPLICATE_VALUE, duplicate value found: Custom_Mapping_Name__c')){
            pretty = 'Custom Mapping Name must be unique, a Custom Mapping with the name ' + customMapping.Custom_Mapping_Name__c + ' already exists'; 
        }
        return pretty;
    }

    public List<Object_Relationship__c> needsUpdating(Map<Id, Object_Relationship__c> oldObjectRels, Map<Id, Object_Relationship__c> objectRelsToCheck){
        Set<String> fieldsToCheck = new Set<String>{
            'Map_Record_Type__c', 'Active__c', 'Copy_Attachments__c','Order__c','Copy_Notes__c','Target_Relationship_Id__c','Related_Record_Parent_Id__c','Parent_Object_Relationship__c', 'Target_Parent_Lookup__c'
        };
        return (List<Object_Relationship__c>) SL_util_DiffRecord.getRecordsWithDiff(oldObjectRels, objectRelsToCheck, fieldsToCheck).values();
    }

    public List<Field_Mapping__c> needsUpdating(Map<Id, Field_Mapping__c> oldFieldMaps, Map<Id, Field_Mapping__c> fieldMapsToCheck){
        Set<String> fieldsToCheck = new Set<String>{'Context_Field_Name__c', 'Target_Field_Name__c', 'Target_Value__c', 'Active__c'};
        return (List<Field_Mapping__c>) SL_util_DiffRecord.getRecordsWithDiff(oldFieldMaps, fieldMapsToCheck, fieldsToCheck).values();
    }

    public void checkAll(){
        for (SL_ObjectRelationshipWrapper objectRelationshipWrapper: objectRelationships){
            if (objectRelationshipWrapper.checkAllStatus!=allCheckboxSelect.get(objectRelationshipWrapper))
            {
                objectRelationshipWrapper.checkAll();
                allCheckboxSelect.put(objectRelationshipWrapper, objectRelationshipWrapper.checkAllStatus);
            }
        }
    }

    public PageReference collapseAll(){
        return null;
    }

    // discard all changes and return them to the current custom mapping with no changes saved
    public PageReference performCancel(){
        PageReference freshPage = Page.SL_CA_Mapping;
        String mapId = '';
        if (customMapping!=null&&customMapping.Id!=null)
        {
            freshPage.getParameters().put('id', customMapping.Id);
        }
        freshPage.getParameters().put('cancel', 'true');
        freshPage.setRedirect(true);
        return freshPage;
    }
    // returns the label of the currently selected object
    public String getObjectName(){
        if (selectedObject==''||Schema.getGlobalDescribe().get(selectedObject)==null){
            return '';
        }
        return Schema.getGlobalDescribe().get(selectedObject).getDescribe().getLabel();
    }

    public void fillCustomMappingName(){
        customMapping.Custom_Mapping_Name__c = customMapping.Name; 
    }
}