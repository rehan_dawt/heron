@isTest
global class SL_AutoCompleteNoQuery_Controller_Test 
{
	@isTest 
	global static void testAutoCompleteNoQuery()
	{
		List<String> queryMatches = new List<String>{'Test1', 'Test2'};
		List<String> results = new List<String>();
		String strSearch = 'Test1';
		
		results = SL_AutoCompleteNoQuery_Controller.getStrings(queryMatches, strSearch);
	}
}