public with sharing class s1_TestUtils{

    public static User getStandardTestUser() {
        try
        {
            String unique = String.valueOf(DateTime.now().getTime());
            Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
            User u = new User(Alias = 'standt', Email = 'standarduser55@testorg.com',
                              EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                              LocaleSidKey = 'en_US', ProfileId = p.Id,
                              TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser@' + unique + 'testorg.com');
            insert u;
            return u;
        }
        catch(Exception e)
        {
            system.debug('>>>>>>>>Error Message in getStandardTestUser fucntion>>>>>>>>>>>' + e.getMessage() + '>>>>>>>>at Line Number>>>>>>>>' + e.getLineNumber());
            return null;
        }
    }

    public static Account getAccount() {
        try
        {
            Account a = new Account();
            String unique = String.valueOf(DateTime.now().getTime());
            a.name = 'foo company ' + unique;
            insert a;
            return a;
        }
        catch(Exception e)
        {
            system.debug('>>>>>>>>Error Message in getAccount fucntion>>>>>>>>>>>' + e.getMessage() + '>>>>>>>>at Line Number>>>>>>>>' + e.getLineNumber());
            return null;
        }
    }

}