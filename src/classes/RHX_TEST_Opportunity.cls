@isTest(SeeAllData=true)
public class RHX_TEST_Opportunity 
{
    @isTest 
    static void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
            FROM Opportunity LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Opportunity(Name='Test Opportunity', StageName='In Progress', CloseDate=date.today())
            );
        }
        
        system.assertEquals(sourceList.size(), 1);
        
        rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}