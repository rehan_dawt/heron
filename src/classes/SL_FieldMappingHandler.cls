public with sharing class SL_FieldMappingHandler {

    private Boolean isExecuting;
    private Integer batchSize;
    public SL_FieldMappingHandler(Boolean isExecuting, Integer batchSize) {
        this.isExecuting = isExecuting;
        this.batchSize = batchSize;
    }

    public void onBeforeInsert(List<Field_Mapping__c> fieldMappingsToInsert){
        SL_MappingValidationUtils.fieldMappingValidate(fieldMappingsToInsert);
    }

    public void onBeforeUpdate(Map<Id, Field_Mapping__c> oldFieldMappings, Map<Id, Field_Mapping__c> newFieldMappings){
        SL_MappingValidationUtils.fieldMappingValidate(newFieldMappings.values());
    }

}