public with sharing class SL_ObjectRelationshipHandler {

    private Boolean isExecuting;
    private Integer batchSize;
    public SL_ObjectRelationshipHandler(Boolean isExecuting, Integer batchSize) {
        this.isExecuting = isExecuting;
        this.batchSize = batchSize;
    }

    public void onBeforeInsert(List<Object_Relationship__c> objectMappingsToInsert){
        SL_MappingValidationUtils.objectRelationshipValidate(objectMappingsToInsert);
    }

    public void onBeforeUpdate(Map<Id, Object_Relationship__c> oldObjectRelationships, Map<Id, Object_Relationship__c> newObjectRelationships){
        SL_MappingValidationUtils.objectRelationshipValidate(newObjectRelationships.values());
    }

}