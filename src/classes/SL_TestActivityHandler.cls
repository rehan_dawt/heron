@isTest
public class SL_TestActivityHandler {

	@isTest 
    static void unitTestCodeTask() {

        SL_ActivityHandler.run = true;

        Task objTask = new Task();
        objTask.IsParent__c = true;
        objTask.ParentId__c = null;
        insert objTask;

        Task objTask1 = new Task();
        objTask1.ParentId__c = objTask.Id;
        objTask1.IsParent__c = false;
        insert objTask1;
		
        objTask.Subject = 'Test Task Subject';
        objTask.IsParent__c = true;
        objTask.ParentId__c = null;
        update objTask;

        delete objTask;
    }
    
    @isTest 
    static void unitTestCodeEvent() {

        Event objEvent = new Event();
        objEvent.IsParent__c = true;
        objEvent.StartDateTime = date.today();
        objEvent.EndDateTime = date.today();
        insert objEvent;

        Event objEvent1 = new Event();
        objEvent1.StartDateTime = date.today();
        objEvent1.EndDateTime = date.today();
        objEvent1.ParentId__c = objEvent.Id;
        objEvent1.IsParent__c = false;
        insert objEvent1;

        objEvent1.Subject = 'Test Event Subject';
        update objEvent1;

        delete objEvent;
    }
}