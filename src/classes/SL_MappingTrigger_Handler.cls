/**
* \arg ClassName      : SL_MappingTrigger_Handler
* \arg JIRATicket     : LIB-123
* \arg CreatedOn      : 2013-10-09
* \arg LastModifiedOn : 2015-02-04
* \arg CreatededBy    : Lodhi
* \arg ModifiedBy     : Edward Rivera
* \arg Description    : This is a legacy class which invokes a legacy method in the SL_Mapping_Handler class to
*                       perform a mapped conversion, inserting all resulting records.
*/
global class SL_MappingTrigger_Handler {
    /* Start Variables */
    /* End Variables*/
    
    /* Start Constructor*/
    global SL_MappingTrigger_Handler(){}
    /* End Contructor */
    /*!
        * MethodName : createSobjectRecords
        * param      : List of Sobject corresponding to which records need to be create and the Sobject name correspond which Custom Mapping records need to query.
        * Description: This method will create the Sobject records on the basis of the Custom Mapping records and his child records and also copy the related list records.
    */
    global void createSobjectRecords(Map<Id,Sobject> mapIdToSobject, String strSobjectAPIName){
        SL_Mapping_Handler handler = new SL_Mapping_Handler();
        handler.generateAndInsertAllRecords(mapIdToSobject, strSobjectAPIName, '');
    }    
}