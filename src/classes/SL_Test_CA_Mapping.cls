@isTest
private class SL_Test_CA_Mapping {

    @isTest 
    static void testController() {
        SL_CA_Mapping controller = new SL_CA_Mapping();
        controller.selectedObject='Account';
        controller.chooseSObject();
        controller.customMapping.Name = 'Account';
        controller.customMapping.Custom_Mapping_Name__c = 'Account';
        controller.sourceObject='Account';
        controller.newTargetObject='Account';
        controller.chooseTarget();
        controller.sourceObject='Opportunity';
        controller.newTargetObject='Account';
        controller.chooseTarget();
        controller.performSave();
        controller.checkAll();
        controller.collapseAll();
        controller.performCancel();
        Object_Relationship__c objRel = [SELECT Id FROM Object_Relationship__c WHERE Context_Object_API__c='Account' LIMIT 1];
        List<Field_Mapping__c> fieldMappings = new List<Field_Mapping__c>();
        fieldMappings.add(new Field_Mapping__c(RecordTypeId=[SELECT Id FROM RecordType WHERE DeveloperName='Reference' AND (SObjectType='Field_Mapping__c' OR SObjectType='SL_Convert__Field_Mapping__c')].Id, Object_Relationship__c=objRel.Id, Context_Field_Name__c='Name', Target_Field_Name__c='Name'));
        fieldMappings.add(new Field_Mapping__c(RecordTypeId=[SELECT Id FROM RecordType WHERE DeveloperName='Value' AND (SObjectType='Field_Mapping__c' OR SObjectType='SL_Convert__Field_Mapping__c')].Id, Object_Relationship__c=objRel.Id, Context_Field_Name__c='Name', Target_Field_Name__c='Name'));
        insert fieldMappings;
        Test.startTest();
        controller.objectRelationships[0].checkAll();
        controller.objectRelationships[0].activateSelected();
        controller.objectRelationships[0].deactivateSelected();
        controller.objectRelationships[0].deleteSelected();
        controller.performSave();
        controller.objectRelationships[0].setDelete();
        Test.stopTest();
        
        system.assertEquals(fieldMappings.size(), 2, 'Expecting Field Mapping records to be inserted');
    }
}