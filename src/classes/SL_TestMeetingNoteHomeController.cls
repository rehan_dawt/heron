@isTest
public class SL_TestMeetingNoteHomeController{

    private static String tooLongAccName = 'LOTS OF '+
        'CHARACTERS XXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'+
        'XXXXXXXXXXXXXXXX'; 
     
    static private void assertError(String jsonResult, String expectedError, String method) {

        system.debug('$$$$$$$$$$$$$$$$$$$$\n\n' + jsonResult);

        List<Object> errorArray = (List<Object>)JSON.deserializeUntyped(jsonResult);
        
        System.assertNotEquals(null, errorArray, 
                               'error array missing from '+method+' result');
        System.assertNotEquals(0, errorArray.size(), 
                               'error array is empty in '+method+' result');
        
        Map<String, Object> error = (Map<String, Object>)errorArray[0];
        String errorCode = (String)error.get('errorCode');
        System.assertNotEquals(null, errorCode, 
                               'errorCode property missing from '+method+' result');
        System.assertEquals(expectedError, errorCode, 
                               'errorCode should be '+expectedError+' in '+method+' result');
    }
    
    @isTest 
    static void testDescribe() {
        // Assume we have accounts
        String jsonResult = SL_MeetingNoteHomeController.describe('Account');
        
        System.assertNotEquals(null, jsonResult, 
                               'SL_MeetingNoteHomeController.describe returned null');
                      
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(jsonResult);
        
        System.assertNotEquals(null, result.get('fields'), 
                               'fields property missing from SL_MeetingNoteHomeController.describe result');

        // TODO - more assertions on describe results
        
        // Invalid object type
        // Hope there isn't a QXZXQZXZQXZQ object type!
        jsonResult = SL_MeetingNoteHomeController.describe('QXZXQZXZQXZQ');
        assertError(jsonResult, 'NOT_FOUND', 'SL_MeetingNoteHomeController.describe');        
    }
    
    static private void assertRecord(Map<String, Object> record, String accName, String accNumber, String method) {
        Map<String, Object> attributes = (Map<String, Object>)record.get('attributes');
        System.assertNotEquals(null, attributes, 
                               'attributes property missing from '+method+' result');
        System.assertNotEquals(0, attributes.keySet().size(), 
                               'empty attributes object in '+method+' result');
        
        String type = (String)attributes.get('type');
        System.assertNotEquals(null, type, 
                               'type property missing from '+method+' result');
        System.assertEquals('Account', type, 
                               'Wrong type in '+method+' result');
        
        String url = (String)attributes.get('url');
        System.assertNotEquals(null, url, 
                               'url property missing from '+method+' result');
       
        Id id = (Id)record.get('Id');
        System.assertNotEquals(null, id, 
                               'Id property missing from '+method+' result');
        Account account = [SELECT Id, Name FROM Account WHERE Id = :id LIMIT 1];
        System.assertNotEquals(null, account, 
                               'Couldn\'t find account record identified by '+method+' result');
        System.assertEquals(accName, account.Name, 
                               'Account name doesn\'t match in '+method+' result');
        
        String name = (String)record.get('Name');
        System.assertNotEquals(null, name, 
                               'Name property missing from '+method+' result');
        System.assertEquals(accName, name, 
                               'Wrong account name in '+method+' result');
   
        String accountNumber = (String)record.get('AccountNumber');
        System.assertNotEquals(null, name, 
                               'AccountNumber property missing from '+method+' result');
        System.assertEquals(accNumber, accountNumber, 
                               'Wrong account number in '+method+' result');
    }

    static private void testRetrieve(String accName, String accNumber, Id id) {
        String jsonResult = SL_MeetingNoteHomeController.retrieve('Account', id, 'id, Name, AccountNumber');
        System.assertNotEquals(null, jsonResult,'SL_MeetingNoteHomeController.retrieve returned null');

        jsonResult = SL_MeetingNoteHomeController.retrieve('Account', id, 'Name, AccountNumber');
        System.assertNotEquals(null, jsonResult,'SL_MeetingNoteHomeController.retrieve returned null');
        
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(jsonResult);
        assertRecord(result, accName, accNumber, 'SL_MeetingNoteHomeController.retrieve');       

        jsonResult = SL_MeetingNoteHomeController.retrieve('YYYYY', id, 'Name, AccountNumber'); 
        
        // TODO - test negative paths for retrieve
    }  

    static private void testQuery(String accName, String accNumber) {
        String jsonResult = SL_MeetingNoteHomeController.query('SELECT Id, Name, AccountNumber FROM Account WHERE Name = \''+accName+'\'');
        
        System.assertNotEquals(null, jsonResult,'SL_MeetingNoteHomeController.query returned null');
        
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(jsonResult);
        
        List<Object> records = (List<Object>)result.get('records');
        System.assertNotEquals(null, records, 
                               'records property missing from SL_MeetingNoteHomeController.query result');
        System.assertEquals(1, records.size(), 
                               'records array should have single record in SL_MeetingNoteHomeController.query result');
        
        Map<String, Object> record = (Map<String, Object>)records[0];
        
        assertRecord(record, accName, accNumber, 'SL_MeetingNoteHomeController.query');        
                
        Integer totalSize = (Integer)result.get('totalSize');
        System.assertNotEquals(null, totalSize, 
                               'totalSize property missing from SL_MeetingNoteHomeController.query result');
        System.assertEquals(1, totalSize, 
                               'totalSize should be 1 in SL_MeetingNoteHomeController.query result');

        Boolean done = (Boolean)result.get('done');
        System.assertNotEquals(null, done, 
                               'done property missing from SL_MeetingNoteHomeController.query result');
        System.assertEquals(true, done, 
                            'done should be true in SL_MeetingNoteHomeController.query result');
        
        jsonResult = SL_MeetingNoteHomeController.query('SELECT Ids, Name FROM Account WHERE Name = \''+accName+'\'');
        assertError(jsonResult, 'INVALID_QUERY', 'SL_MeetingNoteHomeController.query');
    }

    static private void testSearch(String accName, String accNumber, Id id) {
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = id;
        Test.setFixedSearchResults(fixedSearchResults);
        String jsonResult = SL_MeetingNoteHomeController.search('FIND {'+accName+'} IN ALL FIELDS RETURNING Account (Id, Name, AccountNumber)');
        
        System.assertNotEquals(null, jsonResult, 
                               'SL_MeetingNoteHomeController.search returned null');
        
        List<Object> result = (List<Object>)JSON.deserializeUntyped(jsonResult);
        
        List<Object> records = (List<Object>)result[0];
        
        Map<String, Object> record = (Map<String, Object>)records[0];
        
        assertRecord(record, accName, accNumber, 'SL_MeetingNoteHomeController.search'); 
        
    }

    static private void testUpdate(String accName, String accNumber, Id id, String fields) {
        String jsonResult = SL_MeetingNoteHomeController.updat('Account', id, '{"Name":"'+accName+'", "AccountNumber":"'+accNumber+'"}'); 
        //System.assertEquals(null, jsonResult, 'Non-null result from SL_MeetingNoteHomeController.updat');
        Account account = [SELECT Id, Name, AccountNumber FROM Account WHERE Id = :id LIMIT 1];
        System.assertNotEquals(null, account, 'Couldn\'t find account record after SL_MeetingNoteHomeController.updat');
        System.assertEquals(accName, account.Name, 'Account name doesn\'t match after SL_MeetingNoteHomeController.updat');
        System.assertEquals(accNumber, account.AccountNumber, 'Account number doesn\'t match after SL_MeetingNoteHomeController.updat');
        
        jsonResult = SL_MeetingNoteHomeController.updat('QXZXQZXZQXZQ', id, '{"Name":"'+accName+'"}');
        assertError(jsonResult, 'NOT_FOUND', 'SL_MeetingNoteHomeController.updat');
        
        jsonResult = SL_MeetingNoteHomeController.updat('Account', id, '{"XQZXQZXQZXQZ" : "'+accName+'"}');
        assertError(jsonResult, 'INVALID_FIELD', 'SL_MeetingNoteHomeController.updat');

        jsonResult = SL_MeetingNoteHomeController.updat('Account', id, '{"Name" "'+accName+'"}');
        system.debug('###############\n\n' + jsonResult + '\n\n');
        assertError(jsonResult, 'JSON_PARSER_ERROR', 'SL_MeetingNoteHomeController.updat');
                
        jsonResult = SL_MeetingNoteHomeController.updat('Account', id, '{"Name" : "'+tooLongAccName+'"}');
        assertError(jsonResult, 'STRING_TOO_LONG', 'SL_MeetingNoteHomeController.updat');
    }

    static private void testUpsert(String accName, String accNumber, Id id, String fields) {
        String jsonResult = SL_MeetingNoteHomeController.upser('Account', 
                                                     'Id', 
                                                     (String)id, 
                                                     '{"Name":"'+accName+'", '+
                                                     '"AccountNumber":"'+accNumber+'",'+
                                                     fields+'}');

        Account account = [SELECT Id, Name, AccountNumber FROM Account WHERE Id = :id LIMIT 1];
        System.assertNotEquals(null, account, 
                               'Couldn\'t find account record after SL_MeetingNoteHomeController.upser');
        System.assertEquals(accName, account.Name, 
                               'Account name doesn\'t match after SL_MeetingNoteHomeController.upser');
        System.assertEquals(accNumber, account.AccountNumber, 
                               'Account number doesn\'t match after SL_MeetingNoteHomeController.upser');
    }

    static private void testDelete(Id id) {
        String jsonResult = SL_MeetingNoteHomeController.del('QXZXQZXZQXZQ', id);
        assertError(jsonResult, 'NOT_FOUND', 'SL_MeetingNoteHomeController.del');

        jsonResult = SL_MeetingNoteHomeController.del('Account', id); 
        System.assertEquals(null, jsonResult, 
                               'Non-null result from SL_MeetingNoteHomeController.del');
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id = :id];
        System.assertEquals(0, accounts.size(), 
                               'Account record was not deleted by SL_MeetingNoteHomeController.del');

        jsonResult = SL_MeetingNoteHomeController.del('Account', id); 
        assertError(jsonResult, 'ENTITY_IS_DELETED', 'SL_MeetingNoteHomeController.del');
    }
    
    static private Id testCreate(String accName, String accNumber, String fields) {
        // Assume we can create an account 

        // Try with data in correct types
        String jsonResult = SL_MeetingNoteHomeController.create('Account','{"Name": "'+accName+'", '+'"AccountNumber" : "'+accNumber+'",'+fields+'}');
        
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.create returned null');
        
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(jsonResult);
        
        Boolean success = (Boolean)result.get('success');

        System.assertNotEquals(null, success, 'success property missing from SL_MeetingNoteHomeController.create result');

        System.assertNotEquals(false, success, 'success is false in SL_MeetingNoteHomeController.create result');
        
        List<Object> errors = (List<Object>)result.get('errors');

        System.assertNotEquals(null, errors, 'errors property missing from SL_MeetingNoteHomeController.create result');

        System.assertEquals(0, errors.size(), 'errors array is not empty in SL_MeetingNoteHomeController.create result');
        
        Id id = (Id)result.get('id');

        System.assertNotEquals(null, id, 'id property missing from SL_MeetingNoteHomeController.create result');

        Account account = [SELECT Id, Name, AccountNumber FROM Account LIMIT 1];

        System.assertNotEquals(null, account, 'Couldn\'t find account record created by SL_MeetingNoteHomeController.create result');

        System.assertEquals(accName, account.Name, 'Account name doesn\'t match in SL_MeetingNoteHomeController.create result');

        System.assertEquals(accNumber, account.AccountNumber, 'Account number doesn\'t match in SL_MeetingNoteHomeController.create result');
        
        jsonResult = SL_MeetingNoteHomeController.create('QXZXQZXZQXZQ', '{"Name": "'+accName+'"}');

        assertError(jsonResult, 'NOT_FOUND', 'SL_MeetingNoteHomeController.create');
                
        jsonResult = SL_MeetingNoteHomeController.create('Account', '{"Name" "'+accName+'"}');

        assertError(jsonResult, 'JSON_PARSER_ERROR', 'SL_MeetingNoteHomeController.create');
                
        jsonResult = SL_MeetingNoteHomeController.create('Account', '{"XQZXQZXQZXQZ" : "'+accName+'"}');

        assertError(jsonResult, 'INVALID_FIELD', 'SL_MeetingNoteHomeController.create');

        jsonResult = SL_MeetingNoteHomeController.create('Account', '{"Name" : "'+tooLongAccName+'"}');
        
        assertError(jsonResult, 'STRING_TOO_LONG', 'SL_MeetingNoteHomeController.create');

        return id;
    }

    @isTest 
    static void testCRUD() {
        String accName = 'Test1';
        String accNumber = '1234';
        
        // String field values
        Id id = testCreate(accName, accNumber, '"AnnualRevenue" : "1000000",'+
             '"NumberOfEmployees" : "1000",'+
             '"Phone" : "(111) 222-3333"');

        testDelete(id);
        
        // Integer field values
        id = testCreate(accName, accNumber, '"AnnualRevenue" : 1000000,'+
             '"NumberOfEmployees" : 1000,'+
             '"Phone" : "(111) 222-3333"');
        testRetrieve(accName, accNumber, id);
        testQuery(accName, accNumber);
        testSearch(accName, accNumber, id);
        testUpdate(accName+'1', accNumber+'1', id, '"AnnualRevenue" : "1100000",'+
             '"NumberOfEmployees" : "1100",'+
             '"Phone" : "(112) 222-3333"');
        testUpdate(accName+'2', accNumber+'2', id, '"AnnualRevenue" : "2000000",'+
             '"NumberOfEmployees" : "2000",'+
             '"Phone" : "(222) 222-3333"');
        testUpsert(accName+'3', accNumber+'3', id, '"AnnualRevenue" : 3000000,'+
             '"NumberOfEmployees" : 3000,'+
             '"Phone" : "(333) 222-3333"');
        testUpsert(accName+'4', accNumber+'4', id, '"AnnualRevenue" : 4000000,'+
             '"NumberOfEmployees" : 4000,'+
             '"Phone" : "(444) 222-3333"');

        Account objAccount = new Account(Name='Test');
        insert objAccount;        
 
        Task objTask = new Task();
        objTask.put('isParent__c', true); 
        objTask.WhatId=objAccount.Id;
        insert objTask;

        Task objTask2 = new Task();
        objTask2.put('isParent__c', false); 
        objTask2.WhatId=objAccount.Id;
        objTask2.put('ParentId__c' , objTask.Id);
        insert objTask2;

        Task objTask3 = new Task();
        objTask3.put('isParent__c', true);
        objTask3.WhatId=objAccount.Id;
        objTask3.Status='Completed'; 
        insert objTask3;

        Task objTask4 = new Task();
        objTask4.put('isParent__c', false);
        objTask4.WhatId=objAccount.Id;
        objTask4.Status='Completed'; 
        objTask4.put('ParentId__c', objTask3.Id); 
        insert objTask4;

        Event objEvent = new Event();
        objEvent.WhatId=objAccount.Id;
        objEvent.StartDateTime=Datetime.Now(); 
        objEvent.EndDateTime=Datetime.Now();
        objEvent.put('isParent__c', true);
        insert objEvent;

        Event objEvent2 = new Event();
        objEvent2.WhatId=objAccount.Id;
        objEvent2.StartDateTime=Datetime.Now();
        objEvent2.EndDateTime=Datetime.Now();
        objEvent2.put('ParentId__c' , objEvent.Id);
        objEvent2.put('isParent__c', false);
        insert objEvent2;

        Event objEvent3 = new Event();
        objEvent3.WhatId=objAccount.Id;
        objEvent3.StartDateTime=Datetime.Now().addMinutes(-10);
        objEvent3.EndDateTime=Datetime.Now();
        objEvent3.put('isParent__c' , true);
        insert objEvent3;

        Event objEvent4 = new Event();
        objEvent4.WhatId=objAccount.Id;
        objEvent4.StartDateTime=Datetime.Now().addMinutes(-8); 
        objEvent4.EndDateTime=Datetime.Now();
        objEvent4.put('ParentId__c', objEvent3.Id) ;
        objEvent4.put('isParent__c', false);
        insert objEvent4; 

        SL_MeetingNoteHomeController objCtrl = new SL_MeetingNoteHomeController(); 
    
        List<Schema.FieldSetMember> jsonResultFieldSet = SL_MeetingNoteHomeController.describeFieldSet('Task', 'SL_ActivityRelatedListTaskFieldSet');
        System.assertNotEquals(null, jsonResultFieldSet, 'SL_MeetingNoteHomeController.describeFieldSet returned null');

        String jsonResult = SL_MeetingNoteHomeController.query('SELECT Subject FROM Event WHERE WhatId = \''+objAccount.Id+'\' AND isDeleted = false PLACEHOLDER-OPENACTIVITY');

        jsonResult = SL_MeetingNoteHomeController.query('SELECT Subject FROM Event WHERE WhatId = \''+objAccount.Id+'\' AND isDeleted = false PLACEHOLDER-CLOSEDACTIVITY');

        jsonResult = SL_MeetingNoteHomeController.queryFromFieldSet(objTask.Id, 'SL_ActivityRelatedListTaskFieldSet');
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.queryFromFieldSet returned null');
        
        jsonResult = SL_MeetingNoteHomeController.soqlFromFieldSet('Task', 'SL_ActivityRelatedListTaskFieldSet');
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.soqlFromFieldSet returned null');
        
        jsonResult = SL_MeetingNoteHomeController.getPicklistValues('Task', 'Type');
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.getPicklistValues returned null');

        jsonResult = SL_MeetingNoteHomeController.getObjType(id); 
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.getObjType returned null');

        jsonResultFieldSet = SL_MeetingNoteHomeController.describeFieldSet('XXXX', 'SL_FollowUpActivitiesFieldSet');
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.describeFieldSet returned null');

        jsonResult = SL_MeetingNoteHomeController.describeField('Account', 'Name');
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.describeField returned null');

        Set<String> jsonResultPicklistValues = SL_MeetingNoteHomeController.getPicklistValuesWithFieldName('Task', 'Type');
        System.assertNotEquals(null, jsonResultPicklistValues, 'SL_MeetingNoteHomeController.getPicklistValuesWithFieldName returned null');

        jsonResult = SL_MeetingNoteHomeController.getDateFormat();
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.getDateFormat returned null');

        jsonResult = SL_MeetingNoteHomeController.getTimeFormat();
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.getTimeFormat returned null');

        jsonResult = SL_MeetingNoteHomeController.getGridRecords(true, '', true, '','Today');
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.getGridRecords returned null');

        jsonResult = SL_MeetingNoteHomeController.getGridRecords(true, 'AND (ActivityDate = null OR ActivityDate <: dtTask)', false, '','Today');
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.getGridRecords returned null');

        jsonResult = SL_MeetingNoteHomeController.getGridRecords(true, 'AND ActivityDate = TODAY', true, 'AND (StartDateTime = TODAY OR ActivityDate = TODAY)','');
        System.assertNotEquals(null, jsonResult, 'SL_MeetingNoteHomeController.getGridRecords returned null');

        testDelete(id);      
    }
}