@isTest
private class SL_Test_MappingValidationUtils {
	// to test lists, just create bad data and validate it
	// to test maps, insert good data, make it bad, and then validate it
	private static Custom_Mapping__c customMapping;
	private static Object_Relationship__c objectRelationship;
	private static Integer numRecordsToTest = 1;
	private static Map<String, ID> developerNameToId = new Map<String, ID>();

	private static void testSetup(){
		for (RecordType rt: [SELECT Id, DeveloperName from RecordType WHERE sObjectType IN ('Field_Mapping__c', 'Object_Relationship__c', 'Custom_Mapping__c','SL_Convert__Field_Mapping__c', 'SL_Convert__Object_Relationship__c', 'SL_Convert__Custom_Mapping__c')]){
			developerNameToId.put(rt.DeveloperName, rt.Id);
		}

		customMapping = new Custom_Mapping__c(SObject_Type__c='Account', RecordTypeId=developerNameToId.get('Custom_Mapping'));
		List<Custom_Mapping__c> customMappings = new List<Custom_Mapping__c>{customMapping};
		insert customMappings;
		objectRelationship = new Object_Relationship__c(Custom_Mapping__c=customMapping.Id,Context_Object_API__c='Account',
														Target_Object_API__c='Account', Context_Relationship_Id__c = 'Account', 
														Target_Relationship_Id__c = 'Account',
														RecordTypeId=developerNameToId.get('Direct_Association'));
														
		List<Object_Relationship__c> objectRelationships = new List<Object_Relationship__c>{objectRelationship};
		insert objectRelationships;
	}
	
	@isTest static void testCustomMappingValidation() {
		testSetup();
		List<Custom_Mapping__c> badCustomMappings = new List<Custom_Mapping__c>();
		for (Integer ii = 0; ii < numRecordsToTest; ii++){
			Custom_Mapping__c invalidCustomMapping = new Custom_Mapping__c(Name='TEST', SObject_Type__c='%%%', RecordTypeId=developerNameToId.get('Custom_Mapping'));
			badCustomMappings.add(invalidCustomMapping);
		}
		Boolean threwException = false;
		try {
			insert badCustomMappings;
		}
		catch(Exception ex){
			threwException = true;
		}
		System.assert(threwException, 'Fake sObject passed insertion.');

		customMapping.Name='TEST';
		update customMapping;
	}
	@isTest static void testObjectRelationshipValidation() {
		testSetup();
		List<Object_Relationship__c> badObjectRelationships = new List<Object_Relationship__c>();
		for (Integer ii = 0; ii < numRecordsToTest; ii++){
			Object_Relationship__c invalidObjectRelationship = new Object_Relationship__c(Name='TEST', Context_Object_API__c='%%%', Custom_Mapping__c=customMapping.Id, RecordTypeId=developerNameToId.get('Direct_Association'));
			badObjectRelationships.add(invalidObjectRelationship);
		}
		Boolean threwException = false;
		try{
			insert badObjectRelationships;
		}
		catch(Exception ex){
			threwException = true;
		}
		System.assert(threwException, 'Fake context object passed insertion.');
		objectRelationship.Name='TEST';
		update objectRelationship;
	}

	@isTest static void testFieldMappingValidation() {
		testSetup();
		List<Field_Mapping__c> badFieldMappings = new List<Field_Mapping__c>();
		for (Integer ii = 0; ii < numRecordsToTest; ii++){
			Field_Mapping__c invalidFieldMapping = new Field_Mapping__c(Context_Field_Name__c='%%%', Target_Field_Name__c='%%%', Active__c=true, Object_Relationship__c=objectRelationship.Id, RecordTypeId=developerNameToId.get('Reference'));
			badFieldMappings.add(invalidFieldMapping);
		}
		Boolean threwException = false;
		try {
			insert badFieldMappings;
		}
		catch(Exception ex){
			threwException = true;
		}
		System.assert(threwException, 'Fake context field passed insertion.');
		Field_Mapping__c goodFieldMapping = new Field_Mapping__c(Object_Relationship__c=objectRelationship.Id, Active__c=true, Context_Field_Name__c='Name', Target_Field_Name__c='Name');
		insert goodFieldMapping;
		goodFieldMapping.Target_Field_Name__c='Id';
		update goodFieldMapping;
	}
	
	@isTest static void testCustomMappingValidate() {
		testSetup();
		
		Map <Id, Custom_Mapping__c> customMappings = new Map <Id, Custom_Mapping__c>();
		for(Custom_Mapping__c objCustom_Mapping : [Select Id, SObject_Type__c , RecordTypeId, Active__c from Custom_Mapping__c])
		{
			customMappings.put(objCustom_Mapping.Id, objCustom_Mapping);
		}
		
		SL_MappingValidationUtils.customMappingValidate(customMappings);
	}
	
	@isTest static void testObjectRelationshipValidate() {
		testSetup();
		
		Object_Relationship__c objectRelationship2 = new Object_Relationship__c(Custom_Mapping__c=customMapping.Id,Context_Object_API__c='Account',
														Target_Object_API__c='Account', Context_Relationship_Id__c = 'Account', 
														Target_Relationship_Id__c = 'Account',
														RecordTypeId=developerNameToId.get('Related_List'));
		
		Map <Id, Object_Relationship__c> objectRelationships = new Map <Id, Object_Relationship__c>();
		for(Object_Relationship__c objObject_Relationship : [Select Id, RecordType.DeveloperName, Count_Field_Mappings__c, Active__c, 
																Context_Relationship_Id__c, Copy_Attachments__c, 
																Copy_Notes__c, Custom_Mapping__c, Related_Record_Parent_Id__c, 
																Target_Relationship_Id__c, Target_Parent_Lookup__c, 
																Map_Record_Type__c, Order__c, Parent_Object_Relationship__c, 
																Context_Object_API__c, Target_Object_API__c, Target_Parent_Id__c 
																from Object_Relationship__c])
		{
			objectRelationships.put(objObject_Relationship.Id, objObject_Relationship);
		}
		
		SL_MappingValidationUtils.objectRelationshipValidate(objectRelationships);
	}
	
	@isTest static void testvalidateObjectRelationship() {
		testSetup();
		
		Object_Relationship__c objectRelationship2 = new Object_Relationship__c(Custom_Mapping__c=customMapping.Id,Context_Object_API__c='Account',
														Target_Object_API__c='Account', Context_Relationship_Id__c = 'Account', 
														Target_Relationship_Id__c = 'Account',Active__c = true,Order__c = 2,
														RecordTypeId=developerNameToId.get('Related_List'));
		insert objectRelationship2;
		
		SL_MappingValidationUtils.validateObjectRelationship(objectRelationship2, new Set<Integer>{1});
	}
}