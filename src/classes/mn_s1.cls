public with sharing class mn_s1 {
    ////data
    //Activity to copy
    public static Event parentEvent; 
    public static Task parentTask;

    // needed in case no record types exist
    public Boolean hasRecordType;
    public String activityRecordTypeId {get;set;}

    public SObject activity {get;set;}

    //whatId to Activity
    public static list<Event> events = new list<Event>();
    public static list<Task> tasks = new list<Task>();

    //whoId to activityRelation
    public static list<TaskRelation> trs = new list<TaskRelation>();
    public static list<EventRelation> ers = new list<EventRelation>();

    //what.type to set of whatIds
    public static map<String,set<Id>> typeSets = new map<String,set<Id>>();

    //map of rlID the related records themselves (what/who);
    public static map<Id,list<sObject>> relatedRecords = new map<Id,list<sObject>>();

    ////schema info
    public static Id parentId; 
    public static String type;
    public static String rtName; 
    public static boolean isEvent;
    public static String subSelect; 
    public static Set<String> creatableFields = new set<String>();
    public static String soslWho;
    public static String soslWhat;

    public static set<Id> setRelatedlistRecordIds = new set<Id>();

    ////configuration
    public static map<Id,rlConfiguration> mnrls = new map<Id,rlConfiguration>();

    public static Map<String, Map<String, String>> mnmf = new Map<String, Map<String, String>>();

    public static Map<String, String> mnSobjectRecordTypeCond = new Map<String, String>();
    public static Map<String, set<String>> mapSobjectQueryFields = new Map<String, set<String>>();    

    public mn_s1(ApexPages.StandardController stdController){ 

        if (!Test.isRunningTest()) { 
            stdController.addFields(new List<String>{'ParentId__c'});
        }
        this.activity = (SObject)stdController.getRecord();    
        initialize(); //Intializing variables on page load
    }

    //Function to initialize the variables on load
    private void initialize(){

        checkRecordTypeExists(String.valueOf(((Id)activity.get('Id')).getSobjectType()));  

        //If activity exists 
        if(activity != null && hasRecordType) 
            activityRecordTypeId = (String)activity.get('RecordTypeId');
        else
            activityRecordTypeId = '';    

    }
    
    @RemoteAction
    public static String init(String profileName,String aType,String aRtID,String aParentId,Id aId)
    {        

        map<String,Object> response = new map<String,Object>();
        try
        {
            type = aType;
            parentId=String.isEmpty(aParentId)? (Id)aId:(Id)aParentId;
            if(!String.isEmpty(aRtID))
            {
                Id rtId = (Id)aRtID;
                rtName=[SELECT DeveloperName FROM RecordType WHERE Id = :rtId LIMIT 1].DeveloperName;
            }
            if(type=='Event')
            {
                isEvent=true;
                subSelect = ',(Select RelationID,EventId,Relation.Type From EventRelations WHERE isWhat=false)';
            }
            else 
            {
                isEvent=false;
                subSelect = ',(Select RelationID,TaskId,Relation.Type From TaskWhoRelations)';
            }
            //get createable fields
            getCreatableFields();
            
            //get activities and joiners
            getActivities(buildActivityQuery());

            //get configuration records
            getConfigData(profileName,rtName);

            buildSOSL(true);
            buildSOSL(false);

            //This method should be executed after buildSOSL as we are using record type values calculated in this method.
            runRelatedQuery();

            response.put('typeSets',typeSets);
            response.put('mnrls',mnrls);
            response.put('relatedRecords',relatedRecords);
            response.put('soslWho',soslWho);
            response.put('soslWhat',soslWhat);
            response.put('mnmf',mnmf);
            response.put('creatableFields',creatableFields);
            
            if(isEvent)
            {
                response.put('joiners',ers);
                response.put('acts',events);
                response.put('parent',parentEvent);
            }else{
                response.put('joiners',trs);
                response.put('acts',tasks);
                response.put('parent',parentTask);
            }

            return JSON.serialize(response);
        }
        catch(Exception e)
        {   
            response.put('exception', true);
            response.put('message', e.getMessage());
            response.put('lineNumber', e.getLineNumber());
            response.put('stackTrace', e.getStackTraceString());
            return JSON.serialize(response);
            throw e;
        }
    }  

    public static void buildSOSL(Boolean isWho)
    {
        map<String,set<String>> soFields = new map<String,set<String>>();
        map<String,set<String>> sobjectRecordTypesTobeIncluded = new map<String,set<String>>();
        map<String,set<String>> sobjectRecordTypesTobeExcluded = new map<String,set<String>>();
        set<String> setSobjectsWithoutRecordTypeCondtion = new set<String>();

        list<String> fieldClause = new list<String>();

        for(rlConfiguration rlc: mnrls.values())
        {   
            if(isWho != rlc.isWho)
            {
                continue;
            }

            //To store the query fields
            if(!soFields.keySet().contains(rlc.sObjectAPIName))
            {
                soFields.put(rlc.sObjectAPIName,new set<String>());
            }
            soFields.get(rlc.sObjectAPIName).addAll(rlc.qFields);

            //To store record types to be included
            if(!sobjectRecordTypesTobeIncluded.keySet().contains(rlc.sObjectAPIName)) {

                sobjectRecordTypesTobeIncluded.put(rlc.sObjectAPIName,new set<String>());
            }
            if(rlc.mnrl.get('RecordType__c') != null) {

                sobjectRecordTypesTobeIncluded.get(rlc.sObjectAPIName).addAll(((String)rlc.mnrl.get('RecordType__c')).split(',',0));
            }            

            //To store record types to be excluded
            if(!sobjectRecordTypesTobeExcluded.keySet().contains(rlc.sObjectAPIName)) {

                sobjectRecordTypesTobeExcluded.put(rlc.sObjectAPIName,new set<String>());
            }
            if(rlc.mnrl.get('Record_Types_To_Be_Excluded__c') != null) {

                sobjectRecordTypesTobeExcluded.get(rlc.sObjectAPIName).addAll(((String)rlc.mnrl.get('Record_Types_To_Be_Excluded__c')).split(',',0));
            }

            //If no record type filters provided
            if(rlc.mnrl.get('RecordType__c') == null && rlc.mnrl.get('Record_Types_To_Be_Excluded__c') == null) {

                setSobjectsWithoutRecordTypeCondtion.add(rlc.sObjectAPIName);
            }
        }

        //Store the fields globally to access in other functions
        mapSobjectQueryFields.putAll(soFields);

        for(String soName: soFields.keyset())
        {
            List<String> qfields = new list<String>(soFields.get(soName));

            String recordTypeCond = '';

            if(!setSobjectsWithoutRecordTypeCondtion.contains(soName)) {

                //All are including (where only RecordTypes to be Included fields are populated)
                if(sobjectRecordTypesTobeIncluded.get(soName).size()>0 && sobjectRecordTypesTobeExcluded.get(soName).size() == 0) {

                    List<String> recordTypes = new list<String>(sobjectRecordTypesTobeIncluded.get(soName));
                    recordTypeCond = 'Where RecordType.DeveloperName IN (\''+String.join(recordTypes, '\', \'')+'\')';

                    mnSobjectRecordTypeCond.put(soName, 'RecordType.DeveloperName IN (\''+String.join(recordTypes, '\', \'')+'\')');
                }

                //All are excluding (where only RecordTypes to be excluded fields are populated)
                if(sobjectRecordTypesTobeIncluded.get(soName).size() == 0 && sobjectRecordTypesTobeExcluded.get(soName).size()>0) {

                    List<String> recordTypes = new list<String>(sobjectRecordTypesTobeExcluded.get(soName));
                    recordTypeCond = 'Where RecordType.DeveloperName NOT IN (\''+String.join(recordTypes, '\', \'')+'\')';

                    mnSobjectRecordTypeCond.put(soName, 'RecordType.DeveloperName NOT IN (\''+String.join(recordTypes, '\', \'')+'\')');
                }

                //Mix of Include and exclude
                if(sobjectRecordTypesTobeIncluded.get(soName).size()>0 && sobjectRecordTypesTobeExcluded.get(soName).size()>0) {

                    //Remove the common record types from excluded list
                    for(String str:sobjectRecordTypesTobeIncluded.get(soName)) {

                        if(sobjectRecordTypesTobeExcluded.get(soName).contains(str)) {

                            sobjectRecordTypesTobeExcluded.get(soName).remove(str);
                        }
                    }

                    List<String> recordTypes = new list<String>(sobjectRecordTypesTobeExcluded.get(soName)); 

                    if(recordTypes.size()>0) {

                        recordTypeCond = 'Where RecordType.DeveloperName NOT IN (\''+String.join(recordTypes, '\', \'')+'\')';

                        mnSobjectRecordTypeCond.put(soName, 'RecordType.DeveloperName NOT IN (\''+String.join(recordTypes, '\', \'')+'\')');
                    }
                }
            }            

            fieldClause.add(soName+'('+String.join(qFields,',')+' '+recordTypeCond+' LIMIT 10)');
        }

        if(isWho) {

            if(!fieldClause.isEmpty())
                soslWho = 'RETURNING '+ String.join(fieldClause,',');

        } else { 

            if(!fieldClause.isEmpty())
                soslWhat = 'RETURNING '+ String.join(fieldClause,',');
        }
    }
    public static void runRelatedQuery()
    {
        for(String soName:typeSets.keySet())
        {
            for(rlConfiguration rlc:mnrls.values())
            {
                set<Id> qIds = typeSets.get(soName);
                if(rlc.sObjectAPIName==soName)
                {
                    relatedRecords.put(rlc.mnRLId,rlc.getRecords(qIds));
                }
            }
        } 
    }

    private static void getCreatableFields() 
    {
        Schema.SObjectType oType = (type == 'Event')? Event.sObjectType: Task.sObjectType;
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = oType.getDescribe().Fields.getMap();

        for (Schema.SObjectField ft : fMap.values())
        { 
        // loop through all field tokens (ft)

            Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
            
            if (fd.isCreateable() && !fd.isUnique() && fd.isUpdateable()){ // field is creatable
                creatableFields.add(fd.getName());
            }            
        }
    }
    private static String buildActivityQuery()
    {
        List<String> fieldList = new list<String>(creatableFields);
        fieldList.add('What.Type');
        //make query
        String queryString = 'SELECT '+String.join(fieldList,',');
        queryString+=subSelect;
        queryString+= ' FROM '+type+' WHERE parentID__c =\''+parentId+'\'';
        queryString+= ' OR Id=\''+parentId+'\'';
        return queryString;

    }
    private static void getActivities(String queryString) 
    {

        //queryString
        if(isEvent)
        {
            List<Event> le = database.query(queryString);
            structureData(le);
        }
        else 
        {
            List<Task> lt = database.query(queryString);
            structureData(lt);
        }
    }
    private static void structureData(List<Event> le)
    {
        for(Event e:le)
        {
            //add to whatId2activity map
            events.add(e);
            //add to type2idSet map
            constructTypeSets(e,'What');
            //look for the parentActivity
            if(e.get('ParentId__c') ==null)//we found the parent!
            {
                //assign the parent
                parentEvent = e;
                //add to whatId2activity map
                for(EventRelation ewr:e.EventRelations)
                {
                    EventRelation er = new EventRelation(Id=ewr.Id,EventId=ewr.EventId,RelationId=ewr.RelationID);
                    ers.add(er);
                    //add to type2idSet map
                    constructTypeSets(ewr,'Relation');
                }
            }
        }
    }

    //creates whatId2activity and whoId2activityRelation 
    private static void structureData(List<Task> lt)
    {
        for(Task t:lt)
        {
            //add to whatId2activity map
            tasks.add(t);
            constructTypeSets(t,'What');
            if(t.get('ParentId__c') ==null)//we found the parent!
            {
                //assign the parent
                parentTask = t;
                //add the joiners
                for(TaskWhoRelation twr:t.TaskWhoRelations)
                {
                    TaskRelation tr = new TaskRelation(Id=twr.Id,TaskId=twr.TaskId,RelationID=twr.RelationID);
                    trs.add(tr);
                    constructTypeSets(twr,'Relation');
                }
            }
        }
    }
    //helper for structuredata
    private static void constructTypeSets(sObject act,String foreignReference)
    {
        sObject rso = act.getSobject(foreignReference);
        if(rso!=null&&rso.get('type')!=null)
        {
            String polytype = (String)rso.get('type'); 
            Id fkId = (Id)act.get(foreignReference+'Id');

            if(!typeSets.keySet().contains(polytype))
            {
                typeSets.put(polytype,new set<Id>());
            }
            typeSets.get(polytype).add(fkId);
        }

    }
    private static map<Id,rlConfiguration> getConfigData(String pName,String rName)
    {
        //Using get(), put() and dynamic queries to remove common dependencies while creating separate packages
        
        List<SObject> mnpl = new List<SObject>();//To get MN_Page_Layout__c

        List<SObject> mnjs = new List<SObject>();//To get MN_Joiner__c
        
        if(rName!=null) {

            mnpl = Database.query('SELECT Id FROM MN_Page_Layout__c WHERE RecordType__c = :rName AND Profile__c=:pName AND Activity_Type__c =:type LIMIT 1');

            if(!mnpl.isEmpty()) {

                Id mnplId = mnpl[0].Id;

                mnjs = Database.query('SELECT MN_Related_List__r.ObjectName__c,MN_Related_List__r.Order__c,MN_Related_List__r.Record_Types_To_Be_Excluded__c,MN_Related_List__r.RecordType__c, MN_Related_List__r.Mobile_Fields__c , MN_Related_List__r.Rollup_API_Name__c FROM MN_Joiner__c WHERE MN_Page_Layout__r.Id = :mnplId');
            }
        }        

        if(rName==null || mnpl.isEmpty() || mnjs.isEmpty()) {

            String masterString = isEvent?'Master_Event':'Master_Task';
            mnpl = Database.query('SELECT Id FROM MN_Page_Layout__c WHERE Name = :masterString LIMIT 1');

            if(!mnpl.isEmpty()) {

                Id mnplId = mnpl[0].Id;

                mnjs = Database.query('SELECT MN_Related_List__r.ObjectName__c,MN_Related_List__r.Order__c,MN_Related_List__r.Record_Types_To_Be_Excluded__c,MN_Related_List__r.RecordType__c, MN_Related_List__r.Mobile_Fields__c, MN_Related_List__r.Rollup_API_Name__c FROM MN_Joiner__c WHERE MN_Page_Layout__r.Id = :mnplId');
            }            
        }        

        for(SObject mnj:mnjs) {//Iterate over MN_Joiner__c records

            SObject mnrl = mnj.getSObject('MN_Related_List__r');// To get MN_Related_List__c

            mnrls.put(mnrl.Id,new rlConfiguration(mnrl));
        }

        return mnrls;
    } 

    public class rlConfiguration
    {
        //Using get(), put() and dynamic queries to remove common dependencies while creating separate packages

        set<String> sobjectsWithoutNameField = new set<String>{'Contract','Case','Solution'};
        set<String> qFields = new set<String>();
        set<String> viewFields = new set<String>();
        SObject mnrl;//To get MN_Related_List__c
        String sObjectAPIName;
        String sObjectName;
        Id mnrlId;
        String soqlQuery;
        String sosl;
        Boolean isWho;
        //MN_Related_List__c as an argument
        rlConfiguration(SObject amnrl)
        {
            //get query/view fields
            mnrl = amnrl;

            //Add required fields
            if(!sobjectsWithoutNameField.contains((String)mnrl.get('ObjectName__c'))) {

                qFields.add('Name');
                viewFields.add('Name');

            } else if((String)mnrl.get('ObjectName__c') == 'Case' || (String)mnrl.get('ObjectName__c') == 'Contract') {

                qFields.add((String)mnrl.get('ObjectName__c')+'Number');
                viewFields.add((String)mnrl.get('ObjectName__c')+'Number');

            } else if((String)mnrl.get('ObjectName__c') == 'Solution') {

                qFields.add('SolutionName');
                viewFields.add('SolutionName');
            }

            if(mnrl.get('Mobile_Fields__c')!=null)
            {
                viewFields.addAll(((String)mnrl.get('Mobile_Fields__c')).split(',[ ]*',0));
                qFields.addAll(((String)mnrl.get('Mobile_Fields__c')).split(',[ ]*',0));
            }

            /* Populating RollupApi values regardless of activity custom fields and source fields present in pageLayout*/
            if(mnrl.get('Rollup_API_Name__c') != null && mnrl.get('Rollup_API_Name__c') != '') {

                set<String> rollupFields = new set<String>();
                rollupFields.addAll(((String)mnrl.get('Rollup_API_Name__c')).split(',[ ]*',0));

                for(String str : rollupFields) {

                    List<String> sFields = new List<String>();
                    sFields.addAll(str.split(':'));

                    if(sFields[sFields.size()-1] != null && sFields[sFields.size()-1] != '' && sFields.size() == 2) {
                        Boolean result = qFields.contains(sFields[sFields.size()-1]);
                        if(result == false) {
                            qFields.add(sFields[sFields.size()-1]);
                        }
                   }                             
                }
            }

            mnmf.put(mnrl.Id, describeSearchFields((String)mnrl.get('ObjectName__c'), viewFields));

            if(mnrl.get('RecordType__c')!=null||mnrl.get('Record_Types_To_Be_Excluded__c')!=null)
            {
                qFields.add('RecordType.DeveloperName');
            }
            
            //other important info
            sObjectAPIName = (String)mnrl.get('ObjectName__c');
            sObjectName = Schema.getGlobalDescribe().get((String)mnrl.get('ObjectName__c')).getDescribe().getLabel();
            mnrlId = mnrl.Id;
            isWho = sObjectAPIName=='Contact'||sObjectAPIName=='Lead'||sObjectAPIName=='User';

        }
        public list<sObject> getRecords(set<Id> qIds)
        {   
            if(mapSobjectQueryFields.get((String)mnrl.get('ObjectName__c')).contains('RecordType.DeveloperName') && !qFields.contains('RecordType.DeveloperName')){

                qFields.add('RecordType.DeveloperName');
            }

            List<String> lqfields = new list<String>(qFields);
            soqlQuery = 'SELECT ' +String.join(lqfields,',') + ' FROM ' +(String)mnrl.get('ObjectName__c')+ ' WHERE Id IN ';

            soqlQuery+= ':qIds AND Id NOT IN:setRelatedlistRecordIds';

            if(mnSobjectRecordTypeCond.get((String)mnrl.get('ObjectName__c')) != null) {
                soqlQuery = soqlQuery + ' AND '+mnSobjectRecordTypeCond.get((String)mnrl.get('ObjectName__c'));
            }

            list<sObject> sos = database.query(soqlQuery);

            for(Sobject sob:sos) {
                
                setRelatedlistRecordIds.add(sob.Id);
            }

            return sos;
        }
    }

    private static Map<String, String> describeSearchFields(String objtype, Set<String> setFieldName) {        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objtype);
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Map<String, String> field = new Map<String, String>();

        for (String key : setFieldName) {  
            if(key != null && key != ''){
                if(!key.contains('.')){
                    Schema.DescribeFieldResult descField = fieldMap.get(key).getDescribe();
                    field.put(key, descField.getLabel());    
                }else{
                    field.put(key, key.split('\\.')[0]);
                }
            }   
        }
        
        return field;
    } 

    private void checkRecordTypeExists(String objtype) {
        hasRecordType = objtype == 'Task' ? Schema.sObjectType.Task.fields.getMap().containsKey('RecordTypeId') : Schema.sObjectType.Event.fields.getMap().containsKey('RecordTypeId');
    }

}