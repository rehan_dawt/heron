public without sharing class SL_ObjectRelationshipWrapper implements Comparable {

    // used for getting the hashcode of a given wrapper
    private static Integer nextHashcode = 0;
    
    public static Id referenceRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName='Reference' AND (SObjectType='Field_Mapping__c' OR SObjectType='SL_Convert__Field_Mapping__c')].Id;
    public static Id valueRecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName='Value' AND (SObjectType='Field_Mapping__c' OR SObjectType='SL_Convert__Field_Mapping__c')].Id;
    public Object_Relationship__c objectRelationship        {get;set;} 
    public List<FieldMappingWrapper> fieldMappings          {get;set;}
    public Boolean isRelatedList                            {get;set;}
    /* Direct Association fields */

    public List<SelectOption> potentialTargetRelationshipId {get;set;}

    /* Related List fields */

    // this maps to the "Related Record Parent Id" field on the object relationship - the label should change to match this
    public List<SelectOption> potentialSourceParentLookup   {get;set;}
    // this maps to the "Target Parent Lookup" field on the object relationship
    public List<SelectOption> potentialTargetParentLookup   {get;set;}
    
    public String errorMessage                              {get;set;}
    public Boolean toDelete                                 {get;set;}
    // toggles whether this relationship has all of its field mappings mass-selected
    public Boolean checkAllStatus                           {get;set;}
    public String targetObjectLabel                         {get;set;}
    public String sourceObjectLabel                         {get;set;}
    public Boolean isMapRecordType                          {get;set;}

    private Map<String, String> targetAPIToLabel;
    
    private String sourceAPIName;
    public String targetParentAPIName                       {get;set;}
    public List<SelectOption> potentialTargetParent         {get;set;}


    // used to return a hashcode that will be used internally in sfdc maps/sets
    private Integer hashcode;
    
    private Map<String, List<String>> mapLabelToLstAPITemp;

     // constructor
    public SL_ObjectRelationshipWrapper(Object_Relationship__c objectRelationship, List<SelectOption> potentialTargetParent, Boolean isRelatedList){
        this.isRelatedList = isRelatedList;
        sourceAPIName = isRelatedList ? objectRelationship.Context_Relationship_Id__c : objectRelationship.Context_Object_API__c;
        this.potentialTargetParent = potentialTargetParent;
        init(objectRelationship);        
    }

    private void init(Object_Relationship__c objectRelationship){
        hashcode = nextHashcode++;
        toDelete = false;
        checkAllStatus = false;
        isMapRecordType = true;
        Map<String, Schema.SObjectType> allSObjects = Schema.getGlobalDescribe();
        this.objectRelationship = objectRelationship; 
        fieldMappings = new List<FieldMappingWrapper>();     
        isMapRecordType = isRecordTypeMappable(sourceAPIName, objectRelationship.Target_Object_API__c);
        sourceObjectLabel = getLabel(sourceAPIName, allSObjects);
        targetObjectLabel = getLabel(objectRelationship.Target_Object_API__c, allSObjects);
        potentialTargetRelationshipId = new List<SelectOption>();
        potentialSourceParentLookup = new List<SelectOption>();
        potentialTargetParentLookup = new List<SelectOption>();

        // if parent object is filled, we can get this, if it's not (relationships haven't been inserted) but the parent object API name has been selected (say there is only one), we can get this
        if (objectRelationship.Parent_Object_Relationship__c != null){
            targetParentAPIName = objectRelationship.Parent_Object_Relationship__r.Target_Object_API__c;
        }
        else if (potentialTargetParent.size()==1){
            targetParentAPIName = potentialTargetParent[0].getValue();
        }

        if (targetParentAPIName != null){
            potentialTargetParentLookup = getLookupFields(objectRelationship.Target_Object_API__c, targetParentAPIName, allSObjects);
            if (potentialTargetParentLookup.size()==1){
                objectRelationship.Target_Parent_Lookup__c = potentialTargetParentLookup[0].getValue();
            }
        }

        if (isRelatedList){
            potentialSourceParentLookup = getLookupFields(objectRelationship.Context_Relationship_Id__c, objectRelationship.Context_Object_API__c, allSObjects);
            if (potentialSourceParentLookup.size()==1){
                objectRelationship.Related_Record_Parent_Id__c = potentialSourceParentLookup[0].getValue();
            }
        }
        else {
            potentialTargetRelationshipId = getLookupFields(objectRelationship.Target_Object_API__c, sourceAPIName, allSObjects);
            if (potentialTargetRelationshipId.size()==1){
                objectRelationship.Target_Relationship_Id__c = potentialTargetRelationshipId[0].getValue();
            }
        }

        // map of API name to label of all fields on the source object
        Map<String, String> mapToOptions = getFieldOptions(sourceAPIName, allSObjects);  
        targetAPIToLabel = new Map<String, String>();
        
        for (String s: mapToOptions.keySet()){
            targetAPIToLabel.put(mapToOptions.get(s), s);
        }
        
        // map of API name to label of all fields on the target object
        Map<String, String> fieldAPIToLabel = getMappableFields(objectRelationship.Target_Object_API__c, allSObjects);

        Set<String> noFieldMappings = new Set<String>(fieldAPIToLabel.keySet());
        Map<String, Field_Mapping__c> existingFieldMappings = new Map<String, Field_Mapping__c>();
        Map<String, Field_Mapping__c> orphanedFieldMappings = new Map<String, Field_Mapping__c>();
        
        // for new object relationships
        if (objectRelationship.Id==null){
            mapLabelToLstAPITemp = new Map<String, List<String>>();

            if (sourceObjectLabel==targetObjectLabel){
                objectRelationship.Name ='Clone ' + sourceObjectLabel;
            }
            else{
                objectRelationship.Name = sourceObjectLabel +' to '+ targetObjectLabel;
            }
            
            for(String field: fieldAPIToLabel.keySet()){
                if(mapLabelToLstAPITemp.containsKey(fieldAPIToLabel.get(field))){
                    mapLabelToLstAPITemp.get(fieldAPIToLabel.get(field)).add(field);
                }
                else{
                    mapLabelToLstAPITemp.put(fieldAPIToLabel.get(field), new List<String>{field});
                }
            }    

            Set <String> sourceOptions = new Set<String>(mapToOptions.values());
            // field mappings that are auto-populated on object relationship creation
            for (String field: fieldAPIToLabel.keySet()){
                Field_Mapping__c newFieldMapping = new Field_Mapping__c(Active__c=true);
                // automatically maps a field if it has a matching API name
                if (sourceOptions.contains(field) && objectRelationship.Target_Relationship_Id__c != field && objectRelationship.Target_Parent_Lookup__c != field){
                    newFieldMapping.Context_Field_Name__c = field;
                }
                fieldMappings.add(new FieldMappingWrapper(newFieldMapping, mapLabelToLstAPITemp.get(fieldAPIToLabel.get(field)).size()>1 ? (fieldAPIToLabel.get(field)+' ['+field+']') : fieldAPIToLabel.get(field), field, mapToOptions, targetAPIToLabel, referenceRecordTypeId, ''));
            }
        }
        // for existing object relationships
        else {
            mapLabelToLstAPITemp = new Map<String, List<String>>();
            for (Field_Mapping__c fieldMapping: objectRelationship.Field_Mappings__r){
                // if record type is reference, otherwise handle it differently, possibly a different constructor
                if (fieldMapping.recordTypeId==referenceRecordTypeId){
                    if (noFieldMappings.contains(fieldMapping.Target_Field_Name__c)){
                        if(mapLabelToLstAPITemp.containsKey(fieldAPIToLabel.get(fieldMapping.Target_Field_Name__c))){
                            mapLabelToLstAPITemp.get(fieldAPIToLabel.get(fieldMapping.Target_Field_Name__c)).add(fieldMapping.Target_Field_Name__c);
                        }
                        else{
                            mapLabelToLstAPITemp.put(fieldAPIToLabel.get(fieldMapping.Target_Field_Name__c), new List<String>{fieldMapping.Target_Field_Name__c});
                        }
                            
                        noFieldMappings.remove(fieldMapping.Target_Field_Name__c);
                        if (!existingFieldMappings.containsKey(fieldMapping.Target_Field_Name__c)){
                            existingFieldMappings.put(fieldMapping.Target_Field_Name__c, fieldMapping);
                        }
                        else{
                            //handle dupes?
                        }
                    }
                    else {
                        if (!orphanedFieldMappings.containsKey(fieldMapping.Target_Field_Name__c)) {
                            orphanedFieldMappings.put(fieldMapping.Target_Field_Name__c, fieldMapping);
                        }
                        else {
                            // handle dupes?
                        }
                    }
                }
                // value field mapping
                else {
                    
                    if(mapLabelToLstAPITemp.containsKey(fieldAPIToLabel.get(fieldMapping.Target_Field_Name__c))){ 
                            mapLabelToLstAPITemp.get(fieldAPIToLabel.get(fieldMapping.Target_Field_Name__c)).add(fieldMapping.Target_Field_Name__c);
                    }
                    else {
                        mapLabelToLstAPITemp.put(fieldAPIToLabel.get(fieldMapping.Target_Field_Name__c), new List<String>{fieldMapping.Target_Field_Name__c});
                    }
                        
                    noFieldMappings.remove(fieldMapping.Target_Field_Name__c);
                    if (!existingFieldMappings.containsKey(fieldMapping.Target_Field_Name__c)){
                        existingFieldMappings.put(fieldMapping.Target_Field_Name__c, fieldMapping);
                    }
                }
                
            }
            // add in the field mapping rows for empty field mappings
            for (String field: noFieldMappings) {
                fieldMappings.add(new FieldMappingWrapper(fieldAPIToLabel.get(field), field, mapToOptions, targetAPIToLabel, referenceRecordTypeId, ''));
            }
            // add in the field mapping rows for existing field mappings
            for (String field: existingFieldMappings.keySet()) {
                fieldMappings.add(new FieldMappingWrapper(existingFieldMappings.get(field), mapLabelToLstAPITemp.get(fieldAPIToLabel.get(field)).size()>1 ? (fieldAPIToLabel.get(field)+' ['+field+']') : fieldAPIToLabel.get(field), field, mapToOptions, targetAPIToLabel, (existingFieldMappings.get(field).RecordTypeId),  existingFieldMappings.get(field).Target_Value__c));
            }
            
            // add in the field mapping rows for field mappings that exist whose source field no longer exists
            for (String field: orphanedFieldMappings.keySet()) {
                fieldMappings.add(new FieldMappingWrapper(orphanedFieldMappings.get(field), field+' no longer exists!', field, true, mapToOptions, targetAPIToLabel, referenceRecordTypeId, ''));
            }
            if (!orphanedFieldMappings.isEmpty()){
                errorMessage='Some field mappings correspond to fields which do not exist.';
            }
        }
        // sort the field mapping table
        fieldMappings.sort();
    }

    public void setPotentialTargetParentLookup(){
        potentialTargetParentLookup = getLookupFields(objectRelationship.Target_Object_API__c, targetParentAPIName, Schema.getGlobalDescribe());
    }

    private Boolean isRecordTypeMappable(String sourceAPIName, String targetAPIName){
        Boolean sourceRecordType = false;
        Boolean targetRecordType = false;
        for (RecordType recordType: [SELECT Id, SObjectType FROM RecordType WHERE SObjectType =: sourceAPIName OR SObjectType =: targetAPIName]){
            if (sourceRecordType && targetRecordType) break;
            sourceRecordType = sourceRecordType || recordType.SObjectType == sourceAPIName;
            targetRecordType = targetRecordType || recordType.SObjectType == targetAPIName;
        }
        return sourceRecordType && targetRecordType;
    }

    private String getLabel(String apiName, Map<String, Schema.SObjectType> allSObjects){
        return allSObjects.get(apiName) != null ? allSObjects.get(apiName).getDescribe().getLabel() : 'ERROR - NO MATCHING OBJECT: '+ (sourceAPIName);
    }

    // this returns a picklist with the fields on one SObject that look up to a second SObject
    private List<SelectOption> getLookupFields(String childAPIName, String parentAPIName, Map<String, Schema.SObjectType> allSObjects){
        List<SelectOption> potential = new List<SelectOption>();
        for (SObjectField field: allSObjects.get(childAPIName).getDescribe().fields.getMap().values()){
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            Set<Schema.SObjectType> referenceTo = new Set<Schema.SObjectType>(fieldDescribe.getReferenceTo());
            if (fieldDescribe.getType()==Schema.DisplayType.Reference && referenceTo.contains(allSObjects.get(parentAPIName))){
                potential.add(new SelectOption(fieldDescribe.getName(), fieldDescribe.getLabel()));
            }
        }
        return potential;
    }
    
    // used for displaying aspects of the Object Relationship directly
    public Object_Relationship__c getRecord(){
        return objectRelationship;
    }
   
    // returns a map (API name to label) of target object fields that can be mapped as reference mappings
    private Map<String, String> getMappableFields (String targetObjectAPIName, Map<String, Schema.SObjectType> allSObjects){
        Map<String, String> mappableFields = new Map<String, String>();
        
        for (SObjectField field: allSObjects.get(targetObjectAPIName).getDescribe().fields.getMap().values()){
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            if (fieldDescribe.isCreateable()){
                mappableFields.put(fieldDescribe.getName(), fieldDescribe.getLabel());
            }   
        }
        return mappableFields;
    }

    // returns a map (API name to label) of target object fields that can be selected as targets
    private Map<String, String> getFieldOptions(String contextObjectAPIName, Map<String, Schema.SObjectType> allSObjects){
        Map<String, String> possibleMappings = new Map<String, String>();
        try {
            Map <String, String> optionalFields = new Map<String, String>();
            
            if (allSObjects.get(contextObjectAPIName)==null){
                throw new SL_CustomMappingException(contextObjectAPIName);
            }
            
            for(SObjectField field: allSObjects.get(contextObjectAPIName).getDescribe().fields.getMap().values()){
                Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
                if (fieldDescribe.isCreateable()){
                    optionalFields.put(fieldDescribe.getName(), fieldDescribe.getLabel());
                }
            }
             
            List<String> optionalFieldList = new List<String>(optionalFields.keySet());
            optionalFieldList.sort();
            mapLabelToLstAPITemp = new Map<String, List<String>>();
            for(String field: optionalFieldList){
                if(mapLabelToLstAPITemp.containsKey(optionalFields.get(field)))
                    mapLabelToLstAPITemp.get(optionalFields.get(field)).add(field);
                else
                    mapLabelToLstAPITemp.put(optionalFields.get(field), new List<String>{field});
            }
            for (String field: optionalFieldList){
                possibleMappings.put(mapLabelToLstAPITemp.get(optionalFields.get(field)).size()>1 ? (optionalFields.get(field) +' ['+ field +'] ') : optionalFields.get(field), field);
            }
        }   
        catch (SL_CustomMappingException ex){
            System.debug(LoggingLevel.error, 'INVALID TARGET OBJECT: '+ ex.getMessage());
        }   
        return possibleMappings;
    }

    // returns a list of field mappings that need upserting
    public List<Field_Mapping__c> getFieldMappingsToUpsert(){
        List<Field_Mapping__c> fieldMappingRecords = new List<Field_Mapping__c>();
        for (FieldMappingWrapper fieldMappingWrapper: fieldMappings){
            Field_Mapping__c fieldMapping = fieldMappingWrapper.fieldMapping;
            if (fieldMapping.Id==null){
                fieldMapping.Object_Relationship__c = objectRelationship.Id;
            }
            if ( (fieldMapping.Context_Field_Name__c != '' && fieldMapping.Context_Field_Name__c != null && fieldMappingWrapper.isReference) ||  (fieldMapping.Target_Value__c != '' && fieldMapping.Target_Value__c != null && !fieldMappingWrapper.isReference)){
                fieldMappingRecords.add(fieldMapping);  
            }   
        }
        return fieldMappingRecords;
    }

    // returns a list of field mappings that should be deleted
    public List<Field_Mapping__c> getFieldMappingsToDelete(){
        List<Field_Mapping__c> fieldMappingRecords = new List<Field_Mapping__c>();

        for (FieldMappingWrapper fieldMappingWrapper: fieldMappings){
            Field_Mapping__c fieldMapping = fieldMappingWrapper.fieldMapping;
            if (fieldMapping.Id != null && !fieldMapping.isDeleted) {
                if ( (fieldMapping.RecordTypeId == referenceRecordTypeId && (fieldMapping.Context_Field_Name__c==null || fieldMapping.Context_Field_Name__c=='')) || (fieldMapping.RecordTypeId != referenceRecordTypeId && (fieldMapping.Target_Value__c==null || fieldMapping.Target_Value__c==''))){
                    fieldMappingRecords.add(fieldMapping);
                }
            }
        }
        return fieldMappingRecords;
    }

    // marks this relationship for deletion; does not perform DML until the user saves the custom mapping
    public void setDelete (){
        toDelete = true;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Deleting relationship '+objectRelationship.Name+'.  Press cancel to recover.')); 
    }
    
    // checks or unchecks all of the field mappings
    public void checkAll(){
        for (FieldMappingWrapper fieldMappingWrapper: fieldMappings){
            if ( (!checkAllStatus) || (fieldMappingWrapper.fieldMapping.Context_Field_Name__c!=null&&fieldMappingWrapper.fieldMapping.Context_Field_Name__c!='')){
                fieldMappingWrapper.selected=checkAllStatus;
            } 
        }
    }

    // activates all selected field mappings
    public void activateSelected(){
        for (FieldMappingWrapper fieldMappingWrapper: fieldMappings){
            if (fieldMappingWrapper.selected){
                fieldMappingWrapper.fieldMapping.Active__c=true;
            }
        }
    }

    // deactivates all selected field mappings
    public void deactivateSelected(){
        for (FieldMappingWrapper fieldMappingWrapper: fieldMappings){
            if (fieldMappingWrapper.selected)
            {
                fieldMappingWrapper.fieldMapping.Active__c=false;
            }
        }
    }
    
    // deletes all selected field mappings, does not take effect until object relationship or custom mapping is saved
    public void deleteSelected(){
        for (FieldMappingWrapper fieldMappingWrapper: fieldMappings){
            if (fieldMappingWrapper.selected){
                fieldMappingWrapper.fieldMapping.Context_Field_Name__c='';
            }
        }
    }

    // used to place this class in a map
    public Boolean equals(Object obj){
        return obj != null && objectRelationship.Name == ((SL_ObjectRelationshipWrapper) obj).objectRelationship.Name;
    }

    // used to place this class in a map
    public Integer hashcode(){
        return hashcode;
    }

    // for Comparable
    public Integer compareTo(Object compareTo) {
            SL_ObjectRelationshipWrapper compareToORW = (SL_ObjectRelationshipWrapper)compareTo;
            Integer comparison = 0;
            if (objectRelationship.Order__c!=null && compareToORW.objectRelationship.Order__c!=null) {
                if (objectRelationship.Order__c < compareToORW.objectRelationship.Order__c) comparison = -1;
                else if (objectRelationship.Order__c > compareToORW.objectRelationship.Order__c) comparison = 1;
            }
            return comparison; 
    }

    public class FieldMappingWrapper implements Comparable{
        public Field_Mapping__c fieldMapping            {get;set;} 
        public Map<String, String> mapToOptions         {get;set;}
        public Map<String, String> targetAPItoLabel     {get;set;}
        public List<String> mappingOptions              {get;set;}
        public String targetSelection                   {
            get {
                return targetAPItoLabel.get(fieldMapping.Context_Field_Name__c);
            }
            set {
                fieldMapping.Context_Field_Name__c = mapToOptions.get(value);
            }
        }
        public Boolean isReference                      {
            get {
                return fieldMapping.RecordTypeId == SL_ObjectRelationshipWrapper.referenceRecordTypeId;
            }
            private set;
        }

        public String contextFieldLabel                 {get;set;}
        public Boolean error                            {get;set;}
        public Boolean selected                         {get;set;}

        public FieldMappingWrapper(Field_Mapping__c mapping, Boolean error, Map<String, String> mapToOptions, Map<String, String> targetAPItoLabel, String strReferenceOrValue, String strValue){
            this.fieldMapping = mapping;
            this.mapToOptions = mapToOptions;
            this.targetAPItoLabel = targetAPItoLabel;
            mappingOptions = new List<String>(mapToOptions.keySet());
            this.error = error;
            this.selected = false;
            fieldMapping.RecordTypeId = strReferenceOrValue;
            fieldMapping.Target_Value__c = strValue;
            if (error){
                this.fieldMapping.Active__c=false;
            }
        }

        public FieldMappingWrapper(Field_Mapping__c mapping, String contextFieldLabel, String contextFieldName, Boolean error, Map<String, String> mapToOptions, Map<String, String> targetAPItoLabel, String strRefOrVal, String strValue){
            this(mapping, error, mapToOptions, targetAPIToLabel, strRefOrVal, strValue);
            this.contextFieldLabel = contextFieldLabel;
            this.fieldMapping.Target_Field_Name__c=contextFieldName;
        }
       
        public FieldMappingWrapper(Field_Mapping__c mapping, String contextFieldLabel, String contextFieldName, Map<String, String> mapToOptions, Map<String, String> targetAPItoLabel, String strRefOrVal, String strValue){
            this(mapping, contextFieldLabel, contextFieldName, false, mapToOptions, targetAPItoLabel, strRefOrVal, strValue);
        }
        public FieldMappingWrapper(String contextFieldLabel, String contextFieldName, Map<String, String> mapToOptions, Map<String, String> targetAPItoLabel, String strRefOrVal, String strValue){
            this(new Field_Mapping__c(Active__c=true), contextFieldLabel, contextFieldName, mapToOptions, targetAPItoLabel, strRefOrVal, strValue);
        }

        public Integer compareTo(Object compareTo) {
            FieldMappingWrapper compareToFMW = (FieldMappingWrapper)compareTo;
            Integer comparison = 0;
            if (contextFieldLabel < compareToFMW.contextFieldLabel) comparison = -1;
            else if (contextFieldLabel > compareToFMW.contextFieldLabel) comparison = 1;
            return comparison;  
        }
    }
}