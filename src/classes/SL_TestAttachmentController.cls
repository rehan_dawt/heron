@isTest
public class SL_TestAttachmentController{

    @isTest 
    static void unitTestCode() {

        SL_TestDataFactory.createTestDataForTaskEventAttachment();
        
        Task objTask = [Select Id from Task limit 1];
        Event objEvent = [Select Id from Event limit 1];

        Attachment objTaskAttachment = [Select Id from Attachment where ParentId =: objTask.Id limit 1];
        Attachment objEventAttachment = [Select Id from Attachment where ParentId =: objEvent.Id limit 1];
        
        ApexPages.currentPage().getParameters().put('id', objTask.Id);
        ApexPages.currentPage().getParameters().put('id', objEvent.Id);

        SL_AttachmentController objAttachmentController = new SL_AttachmentController();
        objAttachmentController.attachments = new List<Attachment>{
                                                                        new Attachment(Name='Test Attachment1', Body=Blob.valueOf('Unit Test Attachment Body1')),
                                                                        new Attachment(Name='Test Attachment2', Body=Blob.valueOf('Unit Test Attachment Body2'))
                                                                };
        objAttachmentController.getAttachments();  
        
        system.assertEquals(objAttachmentController.getAttachments().size(), 1);                                                            

        objAttachmentController.strRemovedAttachments = objTaskAttachment.Id;
        objAttachmentController.save();
        
        objAttachmentController.strRemovedAttachments = objEventAttachment.Id;
        objAttachmentController.save();

        objAttachmentController.sobjId = '';
        objAttachmentController.save();

        objAttachmentController.refreshAttachments();
        
        system.assertEquals(objAttachmentController.lstAttachments.size(), 0);                              
        
    } 
}