/**
* \arg ClassName      : SL_Test_Mapping_Handler
* \arg JIRATicket     : DT-117
* \arg CreatedOn      : 23/10/2013
* \arg CreatedBy      : Hemant
* \arg ModifiedBy     : -
* \arg Description    : This is test class for SL_Mapping_Handler.
*/

@isTest
public class SL_Test_Mapping_Handler 
{
	@isTest 	
    static void TestMappingTriggerHandlerPositiveCase() 
    {
    	SL_TestDataFactory.createTestDataForMapping();
    	
    	Account objAccount = [Select Id from Account limit 1];
    	Contact objContact = [Select Id from Contact limit 1];
    	 
        map<Id,Account> mapIdToAccount = new map<Id,Account>();
        mapIdToAccount.put(objAccount.Id,objAccount);
        SL_MappingTrigger_Handler objMappingTriggerHandler = new SL_MappingTrigger_Handler();
        /// call createSobjectRecords method
        test.startTest();
        objMappingTriggerHandler.createSobjectRecords(mapIdToAccount,'Opportunity');

        test.stopTest();
        
        list<Opportunity> lstOpportunity = [SELECT Id, Name, AccountId FROM Opportunity WHERE AccountId IN : mapIdToAccount.keyset()];
        //system.assertEquals(lstOpportunity.size(),1 );
        
        //Task record is created 
        list<Task> lstTask = [SELECT Id,WhatId FROM Task ];
        //system.assertEquals(lstTask.size(),1 );
        
        /* not working with standard object, will work with custom objects in some cases  */
        list<OpportunityContactRole> lstOpportunityContactRole = [SELECT Id, ContactId FROM OpportunityContactRole WHERE ContactId = : objContact.Id];
        system.assertEquals(lstOpportunityContactRole.size(),0 );

        SL_MappedRecordWrapper wrapperTest = new SL_MappedRecordWrapper(Account.SObjectType.newSObject(), Account.SObjectType.newSObject(), 'Account', 'Account');
        wrapperTest.getOriginatingSObject();
        wrapperTest.getsObjectAPIName();
        wrapperTest.getLookupFieldToParentRecord();
        wrapperTest.addChild(wrapperTest);
        wrapperTest.doNotInsert();
        SL_Mapping_Handler.ContextRecord contextRecord = new SL_Mapping_Handler.ContextRecord();
        contextRecord.Id = objAccount.Id;
        SL_Mapping_Handler.generateAndInsertAllRecords(new List<SL_Mapping_Handler.ContextRecord>{contextRecord});
    }

	@isTest 
    static void TestMappingTriggerHandlerIterativePositiveCase() 
    {
        test.startTest();
        
        SL_TestDataFactory.createTestDataForMapping();
    	
    	Account objAccount = [Select Id from Account limit 1];
    	Contact objContact = [Select Id from Contact limit 1];
    	
    	Object_Relationship__c objObjectRelationship = [Select Id, Map_Record_Type__c from Object_Relationship__c where Name  = 'Object Relationship 1' limit 1];
    	objObjectRelationship.Map_Record_Type__c = false;
    	update objObjectRelationship;
        
        map<Id,Account> mapIdToAccount = new map<Id,Account>();
        mapIdToAccount.put(objAccount.Id,objAccount);
        SL_Mapping_Handler objMappingTriggerHandler = new SL_Mapping_Handler();
        /// call createSobjectRecords method

        Map<Id, SL_MappedRecordWrapper> testRecords = objMappingTriggerHandler.generateRecords(mapIdToAccount,'Opportunity', '');
        objMappingTriggerHandler.insertHierarchy(testRecords.values());

        test.stopTest();
        
        list<Opportunity> lstOpportunity = [SELECT Id, Name, AccountId FROM Opportunity WHERE AccountId IN : mapIdToAccount.keyset()];
        //system.assertEquals(lstOpportunity.size(),1 );
        
        //Task record is created 
        list<Task> lstTask = [SELECT Id,WhatId FROM Task ];
        //system.assertEquals(lstTask.size(),1 );
        
        /* not working with standard object, will work with custom objects in some cases  */
        list<OpportunityContactRole> lstOpportunityContactRole = [SELECT Id, ContactId FROM OpportunityContactRole WHERE ContactId = : objContact.Id];
        //system.assertEquals(lstOpportunityContactRole.size(),0 );
    }
    
    @isTest 	
    static void testGenerateAndInsertAllRecords() 
    {
    	SL_TestDataFactory.createTestDataForMapping();
    	
    	Account objAccount = [Select Id from Account limit 1];
    	Contact objContact = [Select Id from Contact limit 1];
    	 
        map<Id,Account> mapIdToAccount = new map<Id,Account>();
        mapIdToAccount.put(objAccount.Id,objAccount);
        SL_MappingTrigger_Handler objMappingTriggerHandler = new SL_MappingTrigger_Handler();
        /// call createSobjectRecords method
        test.startTest();
        objMappingTriggerHandler.createSobjectRecords(mapIdToAccount,'Opportunity');

        test.stopTest();
        
        list<Opportunity> lstOpportunity = [SELECT Id, Name, AccountId FROM Opportunity WHERE AccountId IN : mapIdToAccount.keyset()];
        //system.assertEquals(lstOpportunity.size(),1 );
        
        //Task record is created 
        list<Task> lstTask = [SELECT Id,WhatId FROM Task ];
        //system.assertEquals(lstTask.size(),1 );
        
        /* not working with standard object, will work with custom objects in some cases  */
        list<OpportunityContactRole> lstOpportunityContactRole = [SELECT Id, ContactId FROM OpportunityContactRole WHERE ContactId = : objContact.Id];
        system.assertEquals(lstOpportunityContactRole.size(),0 );

        SL_MappedRecordWrapper wrapperTest = new SL_MappedRecordWrapper(Account.SObjectType.newSObject(), Account.SObjectType.newSObject(), 'Account', 'Account');
        wrapperTest.getOriginatingSObject();
        wrapperTest.getsObjectAPIName();
        wrapperTest.getLookupFieldToParentRecord();
        wrapperTest.addChild(wrapperTest);
        wrapperTest.doNotInsert();
        SL_Mapping_Handler.ContextRecord contextRecord = new SL_Mapping_Handler.ContextRecord();
        contextRecord.Id = objAccount.Id;
        
        SL_Mapping_Handler objClass = new SL_Mapping_Handler();
        objClass.generateAndInsertAllRecords(contextRecord.Id, 'Opportunity', '');
        
    }
    
    @isTest 	
    static void testGenerateAndInsertAllRecordsWebservice() 
    {
    	SL_TestDataFactory.createTestDataForMapping();
    	
    	Account objAccount = [Select Id from Account limit 1];
    	Contact objContact = [Select Id from Contact limit 1];
    	 
        map<Id,Account> mapIdToAccount = new map<Id,Account>();
        mapIdToAccount.put(objAccount.Id,objAccount);
        SL_MappingTrigger_Handler objMappingTriggerHandler = new SL_MappingTrigger_Handler();
        /// call createSobjectRecords method
        test.startTest();
        objMappingTriggerHandler.createSobjectRecords(mapIdToAccount,'Opportunity');

        test.stopTest();
        
        list<Opportunity> lstOpportunity = [SELECT Id, Name, AccountId FROM Opportunity WHERE AccountId IN : mapIdToAccount.keyset()];
        //system.assertEquals(lstOpportunity.size(),1 );
        
        //Task record is created 
        list<Task> lstTask = [SELECT Id,WhatId FROM Task ];
        //system.assertEquals(lstTask.size(),1 );
        
        /* not working with standard object, will work with custom objects in some cases  */
        list<OpportunityContactRole> lstOpportunityContactRole = [SELECT Id, ContactId FROM OpportunityContactRole WHERE ContactId = : objContact.Id];
        system.assertEquals(lstOpportunityContactRole.size(),0 );

        SL_MappedRecordWrapper wrapperTest = new SL_MappedRecordWrapper(Account.SObjectType.newSObject(), Account.SObjectType.newSObject(), 'Account', 'Account');
        wrapperTest.getOriginatingSObject();
        wrapperTest.getsObjectAPIName();
        wrapperTest.getLookupFieldToParentRecord();
        wrapperTest.addChild(wrapperTest);
        wrapperTest.doNotInsert();
        SL_Mapping_Handler.ContextRecord contextRecord = new SL_Mapping_Handler.ContextRecord();
        contextRecord.Id = objAccount.Id;
        
        SL_Mapping_Handler.generateAndInsertAllRecordsWebservice(new List<SL_Mapping_Handler.ContextRecord>{contextRecord});
        
    }
}